#ifndef IOLPC17XX_H_
#define IOLPC17XX_H_

/*================================================================================================*/
/*
 * System control
 */
#define LPC_SC_EXTINT_EINT0_Pos                  (0)
#define LPC_SC_EXTINT_EINT0_Msk                  (1UL << LPC_SC_EXTINT_EINT0_Pos)

#define LPC_SC_EXTINT_EINT1_Pos                  (1)
#define LPC_SC_EXTINT_EINT1_Msk                  (1UL << LPC_SC_EXTINT_EINT1_Pos)

#define LPC_SC_EXTINT_EINT2_Pos                  (2)
#define LPC_SC_EXTINT_EINT2_Msk                  (1UL << LPC_SC_EXTINT_EINT2_Pos)

#define LPC_SC_EXTINT_EINT3_Pos                  (3)
#define LPC_SC_EXTINT_EINT3_Msk                  (1UL << LPC_SC_EXTINT_EINT3_Pos)


#define LPC_SC_EXTMODE_EXTMODE0_Pos              (0)
#define LPC_SC_EXTMODE_EXTMODE0_Msk              (1UL << LPC_SC_EXTMODE_EXTMODE0_Pos)

#define LPC_SC_EXTMODE_EXTMODE1_Pos              (1)
#define LPC_SC_EXTMODE_EXTMODE1_Msk              (1UL << LPC_SC_EXTMODE_EXTMODE1_Pos)

#define LPC_SC_EXTMODE_EXTMODE2_Pos              (2)
#define LPC_SC_EXTMODE_EXTMODE2_Msk              (1UL << LPC_SC_EXTMODE_EXTMODE2_Pos)

#define LPC_SC_EXTMODE_EXTMODE3_Pos              (3)
#define LPC_SC_EXTMODE_EXTMODE3_Msk              (1UL << LPC_SC_EXTMODE_EXTMODE3_Pos)


#define LPC_SC_EXTPOLAR_EXTPOLAR0_Pos            (0)
#define LPC_SC_EXTPOLAR_EXTPOLAR0_Msk            (1UL << LPC_SC_EXTPOLAR_EXTPOLAR0_Pos)

#define LPC_SC_EXTPOLAR_EXTPOLAR1_Pos            (1)
#define LPC_SC_EXTPOLAR_EXTPOLAR1_Msk            (1UL << LPC_SC_EXTPOLAR_EXTPOLAR1_Pos)

#define LPC_SC_EXTPOLAR_EXTPOLAR2_Pos            (2)
#define LPC_SC_EXTPOLAR_EXTPOLAR2_Msk            (1UL << LPC_SC_EXTPOLAR_EXTPOLAR2_Pos)

#define LPC_SC_EXTPOLAR_EXTPOLAR3_Pos            (3)
#define LPC_SC_EXTPOLAR_EXTPOLAR3_Msk            (1UL << LPC_SC_EXTPOLAR_EXTPOLAR3_Pos)


#define LPC_SC_PCONP_PCTIM0_Pos                  (1)
#define LPC_SC_PCONP_PCTIM0_Msk                  (1UL << LPC_SC_PCONP_PCTIM0_Pos)

#define LPC_SC_PCONP_PCTIM1_Pos                  (2)
#define LPC_SC_PCONP_PCTIM1_Msk                  (1UL << LPC_SC_PCONP_PCTIM1_Pos)

#define LPC_SC_PCONP_PCUART0_Pos                 (3)
#define LPC_SC_PCONP_PCUART0_Msk                 (1UL << LPC_SC_PCONP_PCUART0_Pos)

#define LPC_SC_PCONP_PCUART1_Pos                 (4)
#define LPC_SC_PCONP_PCUART1_Msk                 (1UL << LPC_SC_PCONP_PCUART1_Pos)

#define LPC_SC_PCONP_PCPWM1_Pos                  (6)
#define LPC_SC_PCONP_PCPWM1_Msk                  (1UL << LPC_SC_PCONP_PCPWM1_Pos)

#define LPC_SC_PCONP_PCI2C0_Pos                  (7)
#define LPC_SC_PCONP_PCI2C0_Msk                  (1UL << LPC_SC_PCONP_PCI2C0_Pos)

#define LPC_SC_PCONP_PCSPI_Pos                   (8)
#define LPC_SC_PCONP_PCSPI_Msk                   (1UL << LPC_SC_PCONP_PCSPI_Pos)

#define LPC_SC_PCONP_PCRTC_Pos                   (9)
#define LPC_SC_PCONP_PCRTC_Msk                   (1UL << LPC_SC_PCONP_PCRTC_Pos)

#define LPC_SC_PCONP_PCSSP1_Pos                  (10)
#define LPC_SC_PCONP_PCSSP1_Msk                  (1UL << LPC_SC_PCONP_PCSSP1_Pos)

#define LPC_SC_PCONP_PCADC_Pos                   (12)
#define LPC_SC_PCONP_PCADC_Msk                   (1UL << LPC_SC_PCONP_PCADC_Pos)

#define LPC_SC_PCONP_PCCAN1_Pos                  (13)
#define LPC_SC_PCONP_PCCAN1_Msk                  (1UL << LPC_SC_PCONP_PCCAN1_Pos)

#define LPC_SC_PCONP_PCCAN2_Pos                  (14)
#define LPC_SC_PCONP_PCCAN2_Msk                  (1UL << LPC_SC_PCONP_PCCAN2_Pos)

#define LPC_SC_PCONP_PCGPIO_Pos                  (15)
#define LPC_SC_PCONP_PCGPIO_Msk                  (1UL << LPC_SC_PCONP_PCGPIO_Pos)

#define LPC_SC_PCONP_PCRIT_Pos                   (16)
#define LPC_SC_PCONP_PCRIT_Msk                   (1UL << LPC_SC_PCONP_PCRIT_Pos)

#define LPC_SC_PCONP_PCMCPWM_Pos                 (17)
#define LPC_SC_PCONP_PCMCPWM_Msk                 (1UL << LPC_SC_PCONP_PCMCPWM_Pos)

#define LPC_SC_PCONP_PCQEI_Pos                   (18)
#define LPC_SC_PCONP_PCQEI_Msk                   (1UL << LPC_SC_PCONP_PCQEI_Pos)

#define LPC_SC_PCONP_PCI2C1_Pos                  (19)
#define LPC_SC_PCONP_PCI2C1_Msk                  (1UL << LPC_SC_PCONP_PCI2C1_Pos)

#define LPC_SC_PCONP_PCSSP0_Pos                  (21)
#define LPC_SC_PCONP_PCSSP0_Msk                  (1UL << LPC_SC_PCONP_PCSSP0_Pos)

#define LPC_SC_PCONP_PCTIM2_Pos                  (22)
#define LPC_SC_PCONP_PCTIM2_Msk                  (1UL << LPC_SC_PCONP_PCTIM2_Pos)

#define LPC_SC_PCONP_PCTIM3_Pos                  (23)
#define LPC_SC_PCONP_PCTIM3_Msk                  (1UL << LPC_SC_PCONP_PCTIM3_Pos)

#define LPC_SC_PCONP_PCUART2_Pos                 (24)
#define LPC_SC_PCONP_PCUART2_Msk                 (1UL << LPC_SC_PCONP_PCUART2_Pos)

#define LPC_SC_PCONP_PCUART3_Pos                 (25)
#define LPC_SC_PCONP_PCUART3_Msk                 (1UL << LPC_SC_PCONP_PCUART3_Pos)

#define LPC_SC_PCONP_PCI2C2_Pos                  (26)
#define LPC_SC_PCONP_PCI2C2_Msk                  (1UL << LPC_SC_PCONP_PCI2C2_Pos)

#define LPC_SC_PCONP_PCI2S_Pos                   (27)
#define LPC_SC_PCONP_PCI2S_Msk                   (1UL << LPC_SC_PCONP_PCI2S_Pos)

#define LPC_SC_PCONP_PCGPDMA_Pos                 (29)
#define LPC_SC_PCONP_PCGPDMA_Msk                 (1UL << LPC_SC_PCONP_PCGPDMA_Pos)

#define LPC_SC_PCONP_PCENET_Pos                  (30)
#define LPC_SC_PCONP_PCENET_Msk                  (1UL << LPC_SC_PCONP_PCENET_Pos)

#define LPC_SC_PCONP_PCUSB_Pos                   (31)
#define LPC_SC_PCONP_PCUSB_Msk                   (1UL << LPC_SC_PCONP_PCUSB_Pos)


#define LPC_SC_PCLKSEL0_PCLK_WDT_Pos              (0)
#define LPC_SC_PCLKSEL0_PCLK_WDT_Msk              (3UL << LPC_SC_PCLKSEL0_PCLK_WDT_Pos)

#define LPC_SC_PCLKSEL0_PCLK_TIMER0_Pos           (2)
#define LPC_SC_PCLKSEL0_PCLK_TIMER0_Msk           (3UL << LPC_SC_PCLKSEL0_PCLK_TIMER0_Pos)

#define LPC_SC_PCLKSEL0_PCLK_TIMER1_Pos           (4)
#define LPC_SC_PCLKSEL0_PCLK_TIMER1_Msk           (3UL << LPC_SC_PCLKSEL0_PCLK_TIMER1_Pos)

#define LPC_SC_PCLKSEL0_PCLK_UART0_Pos            (6)
#define LPC_SC_PCLKSEL0_PCLK_UART0_Msk            (3UL << LPC_SC_PCLKSEL0_PCLK_UART0_Pos)

#define LPC_SC_PCLKSEL0_PCLK_UART1_Pos            (8)
#define LPC_SC_PCLKSEL0_PCLK_UART1_Msk            (3UL << LPC_SC_PCLKSEL0_PCLK_UART1_Pos)

#define LPC_SC_PCLKSEL0_PCLK_PWM1_Pos             (12)
#define LPC_SC_PCLKSEL0_PCLK_PWM1_Msk             (3UL << LPC_SC_PCLKSEL0_PCLK_PWM1_Pos)

#define LPC_SC_PCLKSEL0_PCLK_I2C0_Pos             (14)
#define LPC_SC_PCLKSEL0_PCLK_I2C0_Msk             (3UL << LPC_SC_PCLKSEL0_PCLK_I2C0_Pos)

#define LPC_SC_PCLKSEL0_PCLK_SPI_Pos              (16)
#define LPC_SC_PCLKSEL0_PCLK_SPI_Msk              (3UL << LPC_SC_PCLKSEL0_PCLK_SPI_Pos)

#define LPC_SC_PCLKSEL0_PCLK_SSP1_Pos             (20)
#define LPC_SC_PCLKSEL0_PCLK_SSP1_Msk             (3UL << LPC_SC_PCLKSEL0_PCLK_SSP1_Pos)

#define LPC_SC_PCLKSEL0_PCLK_DAC_Pos              (22)
#define LPC_SC_PCLKSEL0_PCLK_DAC_Msk              (3UL << LPC_SC_PCLKSEL0_PCLK_DAC_Pos)

#define LPC_SC_PCLKSEL0_PCLK_ADC_Pos              (24)
#define LPC_SC_PCLKSEL0_PCLK_ADC_Msk              (3UL << LPC_SC_PCLKSEL0_PCLK_ADC_Pos)

#define LPC_SC_PCLKSEL0_PCLK_CAN1_Pos             (26)
#define LPC_SC_PCLKSEL0_PCLK_CAN1_Msk             (3UL << LPC_SC_PCLKSEL0_PCLK_CAN1_Pos)

#define LPC_SC_PCLKSEL0_PCLK_CAN2_Pos             (28)
#define LPC_SC_PCLKSEL0_PCLK_CAN2_Msk             (3UL << LPC_SC_PCLKSEL0_PCLK_CAN2_Pos)

#define LPC_SC_PCLKSEL0_PCLK_ACF_Pos              (30)
#define LPC_SC_PCLKSEL0_PCLK_ACF_Msk              (3UL << LPC_SC_PCLKSEL0_PCLK_ACF_Pos)


#define LPC_SC_PCLKSEL1_PCLK_QEI_Pos              (0)
#define LPC_SC_PCLKSEL1_PCLK_QEI_Msk              (3UL << LPC_SC_PCLKSEL1_PCLK_QEI_Pos)

#define LPC_SC_PCLKSEL1_PCLK_GPIOINT_Pos          (2)
#define LPC_SC_PCLKSEL1_PCLK_GPIOINT_Msk          (3UL << LPC_SC_PCLKSEL1_PCLK_GPIOINT_Pos)

#define LPC_SC_PCLKSEL1_PCLK_PCB_Pos              (4)
#define LPC_SC_PCLKSEL1_PCLK_PCB_Msk              (3UL << LPC_SC_PCLKSEL1_PCLK_PCB_Pos)

#define LPC_SC_PCLKSEL1_PCLK_I2C1_Pos             (6)
#define LPC_SC_PCLKSEL1_PCLK_I2C1_Msk             (3UL << LPC_SC_PCLKSEL1_PCLK_I2C1_Pos)

#define LPC_SC_PCLKSEL1_PCLK_SSP0_Pos             (10)
#define LPC_SC_PCLKSEL1_PCLK_SSP0_Msk             (3UL << LPC_SC_PCLKSEL1_PCLK_SSP0_Pos)

#define LPC_SC_PCLKSEL1_PCLK_TIMER2_Pos           (12)
#define LPC_SC_PCLKSEL1_PCLK_TIMER2_Msk           (3UL << LPC_SC_PCLKSEL1_PCLK_TIMER2_Pos)

#define LPC_SC_PCLKSEL1_PCLK_TIMER3_Pos           (14)
#define LPC_SC_PCLKSEL1_PCLK_TIMER3_Msk           (3UL << LPC_SC_PCLKSEL1_PCLK_TIMER3_Pos)

#define LPC_SC_PCLKSEL1_PCLK_UART2_Pos            (16)
#define LPC_SC_PCLKSEL1_PCLK_UART2_Msk            (3UL << LPC_SC_PCLKSEL1_PCLK_UART2_Pos)

#define LPC_SC_PCLKSEL1_PCLK_UART3_Pos            (18)
#define LPC_SC_PCLKSEL1_PCLK_UART3_Msk            (3UL << LPC_SC_PCLKSEL1_PCLK_UART3_Pos)

#define LPC_SC_PCLKSEL1_PCLK_I2C2_Pos             (20)
#define LPC_SC_PCLKSEL1_PCLK_I2C2_Msk             (3UL << LPC_SC_PCLKSEL1_PCLK_I2C2_Pos)

#define LPC_SC_PCLKSEL1_PCLK_I2S_Pos              (22)
#define LPC_SC_PCLKSEL1_PCLK_I2S_Msk              (3UL << LPC_SC_PCLKSEL1_PCLK_I2S_Pos)

#define LPC_SC_PCLKSEL1_PCLK_RIT_Pos              (26)
#define LPC_SC_PCLKSEL1_PCLK_RIT_Msk              (3UL << LPC_SC_PCLKSEL1_PCLK_RIT_Pos)

#define LPC_SC_PCLKSEL1_PCLK_SYSCON_Pos           (28)
#define LPC_SC_PCLKSEL1_PCLK_SYSCON_Msk           (3UL << LPC_SC_PCLKSEL1_PCLK_SYSCON_Pos)

#define LPC_SC_PCLKSEL1_PCLK_MC_Pos               (30)
#define LPC_SC_PCLKSEL1_PCLK_MC_Msk               (3UL << LPC_SC_PCLKSEL1_PCLK_MC_Pos)

/*
 * Nested Vectored Interrupt Controller
 */
#define LPC_NVIC_ISER0_ISE_WDT_Pos               (0)
#define LPC_NVIC_ISER0_ISE_WDT_Msk               (1UL << LPC_NVIC_ISER0_ISE_WDT_Pos)

#define LPC_NVIC_ISER0_ISE_TIMER0_Pos            (1)
#define LPC_NVIC_ISER0_ISE_TIMER0_Msk            (1UL << LPC_NVIC_ISER0_ISE_TIMER0_Pos)

#define LPC_NVIC_ISER0_ISE_TIMER1_Pos            (2)
#define LPC_NVIC_ISER0_ISE_TIMER1_Msk            (1UL << LPC_NVIC_ISER0_ISE_TIMER1_Pos)

#define LPC_NVIC_ISER0_ISE_TIMER2_Pos            (3)
#define LPC_NVIC_ISER0_ISE_TIMER2_Msk            (1UL << LPC_NVIC_ISER0_ISE_TIMER2_Pos)

#define LPC_NVIC_ISER0_ISE_TIMER3_Pos            (4)
#define LPC_NVIC_ISER0_ISE_TIMER3_Msk            (1UL << LPC_NVIC_ISER0_ISE_TIMER3_Pos)

#define LPC_NVIC_ISER0_ISE_UART0_Pos             (5)
#define LPC_NVIC_ISER0_ISE_UART0_Msk             (1UL << LPC_NVIC_ISER0_ISE_UART0_Pos)

#define LPC_NVIC_ISER0_ISE_UART1_Pos             (6)
#define LPC_NVIC_ISER0_ISE_UART1_Msk             (1UL << LPC_NVIC_ISER0_ISE_UART1_Pos)

#define LPC_NVIC_ISER0_ISE_UART2_Pos             (7)
#define LPC_NVIC_ISER0_ISE_UART2_Msk             (1UL << LPC_NVIC_ISER0_ISE_UART2_Pos)

#define LPC_NVIC_ISER0_ISE_UART3_Pos             (8)
#define LPC_NVIC_ISER0_ISE_UART3_Msk             (1UL << LPC_NVIC_ISER0_ISE_UART3_Pos)

#define LPC_NVIC_ISER0_ISE_PWM_Pos               (9)
#define LPC_NVIC_ISER0_ISE_PWM_Msk               (1UL << LPC_NVIC_ISER0_ISE_PWM_Pos)

#define LPC_NVIC_ISER0_ISE_I2C0_Pos              (10)
#define LPC_NVIC_ISER0_ISE_I2C0_Msk              (1UL << LPC_NVIC_ISER0_ISE_I2C0_Pos)

#define LPC_NVIC_ISER0_ISE_I2C1_Pos              (11)
#define LPC_NVIC_ISER0_ISE_I2C1_Msk              (1UL << LPC_NVIC_ISER0_ISE_I2C1_Pos)

#define LPC_NVIC_ISER0_ISE_I2C2_Pos              (12)
#define LPC_NVIC_ISER0_ISE_I2C2_Msk              (1UL << LPC_NVIC_ISER0_ISE_I2C2_Pos)

#define LPC_NVIC_ISER0_ISE_SPI_Pos               (13)
#define LPC_NVIC_ISER0_ISE_SPI_Msk               (1UL << LPC_NVIC_ISER0_ISE_SPI_Pos)

#define LPC_NVIC_ISER0_ISE_SSP0_Pos              (14)
#define LPC_NVIC_ISER0_ISE_SSP0_Msk              (1UL << LPC_NVIC_ISER0_ISE_SSP0_Pos)

#define LPC_NVIC_ISER0_ISE_SSP1_Pos              (15)
#define LPC_NVIC_ISER0_ISE_SSP1_Msk              (1UL << LPC_NVIC_ISER0_ISE_SSP1_Pos)

#define LPC_NVIC_ISER0_ISE_PLL0_Pos              (16)
#define LPC_NVIC_ISER0_ISE_PLL0_Msk              (1UL << LPC_NVIC_ISER0_ISE_PLL0_Pos)

#define LPC_NVIC_ISER0_ISE_RTC_Pos               (17)
#define LPC_NVIC_ISER0_ISE_RTC_Msk               (1UL << LPC_NVIC_ISER0_ISE_RTC_Pos)

#define LPC_NVIC_ISER0_ISE_EINT0_Pos             (18)
#define LPC_NVIC_ISER0_ISE_EINT0_Msk             (1UL << LPC_NVIC_ISER0_ISE_EINT0_Pos)

#define LPC_NVIC_ISER0_ISE_EINT1_Pos             (19)
#define LPC_NVIC_ISER0_ISE_EINT1_Msk             (1UL << LPC_NVIC_ISER0_ISE_EINT1_Pos)

#define LPC_NVIC_ISER0_ISE_EINT2_Pos             (20)
#define LPC_NVIC_ISER0_ISE_EINT2_Msk             (1UL << LPC_NVIC_ISER0_ISE_EINT2_Pos)

#define LPC_NVIC_ISER0_ISE_EINT3_Pos             (21)
#define LPC_NVIC_ISER0_ISE_EINT3_Msk             (1UL << LPC_NVIC_ISER0_ISE_EINT3_Pos)

#define LPC_NVIC_ISER0_ISE_ADC_Pos               (22)
#define LPC_NVIC_ISER0_ISE_ADC_Msk               (1UL << LPC_NVIC_ISER0_ISE_ADC_Pos)

#define LPC_NVIC_ISER0_ISE_BOD_Pos               (23)
#define LPC_NVIC_ISER0_ISE_BOD_Msk               (1UL << LPC_NVIC_ISER0_ISE_BOD_Pos)

#define LPC_NVIC_ISER0_ISE_USB_Pos               (24)
#define LPC_NVIC_ISER0_ISE_USB_Msk               (1UL << LPC_NVIC_ISER0_ISE_USB_Pos)

#define LPC_NVIC_ISER0_ISE_CAN_Pos               (25)
#define LPC_NVIC_ISER0_ISE_CAN_Msk               (1UL << LPC_NVIC_ISER0_ISE_CAN_Pos)

#define LPC_NVIC_ISER0_ISE_DMA_Pos               (26)
#define LPC_NVIC_ISER0_ISE_DMA_Msk               (1UL << LPC_NVIC_ISER0_ISE_DMA_Pos)

#define LPC_NVIC_ISER0_ISE_I2S_Pos               (27)
#define LPC_NVIC_ISER0_ISE_I2S_Msk               (1UL << LPC_NVIC_ISER0_ISE_I2S_Pos)

#define LPC_NVIC_ISER0_ISE_ENET_Pos              (28)
#define LPC_NVIC_ISER0_ISE_ENET_Msk              (1UL << LPC_NVIC_ISER0_ISE_ENET_Pos)

#define LPC_NVIC_ISER0_ISE_RIT_Pos               (29)
#define LPC_NVIC_ISER0_ISE_RIT_Msk               (1UL << LPC_NVIC_ISER0_ISE_RIT_Pos)

#define LPC_NVIC_ISER0_ISE_MCPWM_Pos             (30)
#define LPC_NVIC_ISER0_ISE_MCPWM_Msk             (1UL << LPC_NVIC_ISER0_ISE_MCPWM_Pos)

#define LPC_NVIC_ISER0_ISE_QEI_Pos               (31)
#define LPC_NVIC_ISER0_ISE_QEI_Msk               (1UL << LPC_NVIC_ISER0_ISE_QEI_Pos)


/*
 * Ethernet
 */
#define LPC_EMAC_MAC1_RECEIVE_ENABLE_Pos         (0)
#define LPC_EMAC_MAC1_RECEIVE_ENABLE_Msk         (1UL << LPC_EMAC_MAC1_RECEIVE_ENABLE_Pos)

#define LPC_EMAC_MAC1_PASS_ALL_RECEIVE_FRAMES_Pos (1)
#define LPC_EMAC_MAC1_PASS_ALL_RECEIVE_FRAMES_Msk (1UL << LPC_EMAC_MAC1_PASS_ALL_RECEIVE_FRAMES_Pos)

#define LPC_EMAC_MAC1_RX_FLOW_CONTROL_Pos        (2)
#define LPC_EMAC_MAC1_RX_FLOW_CONTROL_Msk        (1UL << LPC_EMAC_MAC1_RX_FLOW_CONTROL_Pos)

#define LPC_EMAC_MAC1_TX_FLOW_CONTROL_Pos        (3)
#define LPC_EMAC_MAC1_TX_FLOW_CONTROL_Msk        (1UL << LPC_EMAC_MAC1_TX_FLOW_CONTROL_Pos)

#define LPC_EMAC_MAC1_LOOPBACK_Pos               (4)
#define LPC_EMAC_MAC1_LOOPBACK_Msk               (1UL << LPC_EMAC_MAC1_LOOPBACK_Pos)

#define LPC_EMAC_MAC1_RESET_TX_Pos               (8)
#define LPC_EMAC_MAC1_RESET_TX_Msk               (1UL << LPC_EMAC_MAC1_RESET_TX_Pos)

#define LPC_EMAC_MAC1_RESET_MCS_TX_Pos           (9)
#define LPC_EMAC_MAC1_RESET_MCS_TX_Msk           (1UL << LPC_EMAC_MAC1_RESET_MCS_TX_Pos)

#define LPC_EMAC_MAC1_RESET_RX_Pos               (10)
#define LPC_EMAC_MAC1_RESET_RX_Msk               (1UL << LPC_EMAC_MAC1_RESET_RX_Pos)

#define LPC_EMAC_MAC1_RESET_MCS_RX_Pos           (11)
#define LPC_EMAC_MAC1_RESET_MCS_RX_Msk           (1UL << LPC_EMAC_MAC1_RESET_MCS_RX_Pos)

#define LPC_EMAC_MAC1_SIMULATION_RESET_Pos       (14)
#define LPC_EMAC_MAC1_SIMULATION_RESET_Msk       (1UL << LPC_EMAC_MAC1_SIMULATION_RESET_Pos)

#define LPC_EMAC_MAC1_SOFT_RESET_Pos             (15)
#define LPC_EMAC_MAC1_SOFT_RESET_Msk             (1UL << LPC_EMAC_MAC1_SOFT_RESET_Pos)


#define LPC_EMAC_MAC2_FULL_DUPLEX_Pos            (0)
#define LPC_EMAC_MAC2_FULL_DUPLEX_Msk            (1UL << LPC_EMAC_MAC2_FULL_DUPLEX_Pos)

#define LPC_EMAC_MAC2_FRAME_LENGTH_CHECKING_Pos  (1)
#define LPC_EMAC_MAC2_FRAME_LENGTH_CHECKING_Msk  (1UL << LPC_EMAC_MAC2_FRAME_LENGTH_CHECKING_Pos)

#define LPC_EMAC_MAC2_HUGE_FRAME_ENABLE_Pos      (2)
#define LPC_EMAC_MAC2_HUGE_FRAME_ENABLE_Msk      (1UL << LPC_EMAC_MAC2_HUGE_FRAME_ENABLE_Pos)

#define LPC_EMAC_MAC2_DELAYED_CRC_Pos            (3)
#define LPC_EMAC_MAC2_DELAYED_CRC_Msk            (1UL << LPC_EMAC_MAC2_DELAYED_CRC_Pos)

#define LPC_EMAC_MAC2_CRC_ENABLE_Pos             (4)
#define LPC_EMAC_MAC2_CRC_ENABLE_Msk             (1UL << LPC_EMAC_MAC2_CRC_ENABLE_Pos)

#define LPC_EMAC_MAC2_PAD_CRC_ENABLE_Pos         (5)
#define LPC_EMAC_MAC2_PAD_CRC_ENABLE_Msk         (1UL << LPC_EMAC_MAC2_PAD_CRC_ENABLE_Pos)

#define LPC_EMAC_MAC2_VLAN_PAD_ENABLE_Pos        (6)
#define LPC_EMAC_MAC2_VLAN_PAD_ENABLE_Msk        (1UL << LPC_EMAC_MAC2_VLAN_PAD_ENABLE_Pos)

#define LPC_EMAC_MAC2_AUTO_DETECT_PAD_ENABLE_Pos (7)
#define LPC_EMAC_MAC2_AUTO_DETECT_PAD_ENABLE_Msk (1UL << LPC_EMAC_MAC2_AUTO_DETECT_PAD_ENABLE_Pos)

#define LPC_EMAC_MAC2_PURE_PREAMBLE_ENFORCEMENT_Pos (8)
#define LPC_EMAC_MAC2_PURE_PREAMBLE_ENFORCEMENT_Msk (1UL << \
                                                     LPC_EMAC_MAC2_PURE_PREAMBLE_ENFORCEMENT_Pos)

#define LPC_EMAC_MAC2_LONG_PREAMBLE_ENFORCEMENT_Pos (9)
#define LPC_EMAC_MAC2_LONG_PREAMBLE_ENFORCEMENT_Msk (1UL << \
                                                     LPC_EMAC_MAC2_LONG_PREAMBLE_ENFORCEMENT_Pos)

#define LPC_EMAC_MAC2_NO_BACKOFF_Pos             (12)
#define LPC_EMAC_MAC2_NO_BACKOFF_Msk             (1UL << LPC_EMAC_MAC2_NO_BACKOFF_Pos)

#define LPC_EMAC_MAC2_BACK_PRESSURE_NO_BACKOFF_Pos (13)
#define LPC_EMAC_MAC2_BACK_PRESSURE_NO_BACKOFF_Msk (1UL << \
                                                    LPC_EMAC_MAC2_BACK_PRESSURE_NO_BACKOFF_Pos)

#define LPC_EMAC_MAC2_EXCESS_DEFER_Pos           (14)
#define LPC_EMAC_MAC2_EXCESS_DEFER_Msk           (1UL << LPC_EMAC_MAC2_EXCESS_DEFER_Pos)


#define LPC_EMAC_IPGR_NON_BACK_TO_BACKINTER_PACKET_GAP_PART2_Pos (0)
#define LPC_EMAC_IPGR_NON_BACK_TO_BACKINTER_PACKET_GAP_PART2_Msk (0x7FUL << \
    LPC_EMAC_IPGR_NON_BACK_TO_BACKINTER_PACKET_GAP_PART2_Pos)

#define LPC_EMAC_IPGR_NON_BACK_TO_BACKINTER_PACKET_GAP_PART1_Pos (8)
#define LPC_EMAC_IPGR_NON_BACK_TO_BACKINTER_PACKET_GAP_PART1_Msk (0x7FUL << \
    LPC_EMAC_IPGR_NON_BACK_TO_BACKINTER_PACKET_GAP_PART1_Pos)


#define LPC_EMAC_SUPP_SPEED_Pos                  (8)
#define LPC_EMAC_SUPP_SPEED_Msk                  (1UL << LPC_EMAC_SUPP_SPEED_Pos)


#define LPC_EMAC_MCFG_SCAN_INCREMENT_Pos         (0)
#define LPC_EMAC_MCFG_SCAN_INCREMENT_Msk         (1UL << LPC_EMAC_MCFG_SCAN_INCREMENT_Pos)

#define LPC_EMAC_MCFG_SUPPRESS_PREAMBLE_Pos      (1)
#define LPC_EMAC_MCFG_SUPPRESS_PREAMBLE_Msk      (1UL << LPC_EMAC_MCFG_SUPPRESS_PREAMBLE_Pos)

#define LPC_EMAC_MCFG_CLOCK_SELECT_Pos           (2)
#define LPC_EMAC_MCFG_CLOCK_SELECT_Msk           (0x0FUL << LPC_EMAC_MCFG_CLOCK_SELECT_Pos)

#define LPC_EMAC_MCFG_RESET_MII_MGMT_Pos         (15)
#define LPC_EMAC_MCFG_RESET_MII_MGMT_Msk         (1UL << LPC_EMAC_MCFG_RESET_MII_MGMT_Pos)


#define LPC_EMAC_MCMD_READ_Pos                   (0)
#define LPC_EMAC_MCMD_READ_Msk                   (1UL << LPC_EMAC_MCMD_READ_Pos)

#define LPC_EMAC_MCMD_SCAN_Pos                   (1)
#define LPC_EMAC_MCMD_SCAN_Msk                   (1UL << LPC_EMAC_MCMD_SCAN_Pos)


#define LPC_EMAC_MADR_REGISTER_ADDRESS_Pos       (0)
#define LPC_EMAC_MADR_REGISTER_ADDRESS_Msk       (0x1FUL << LPC_EMAC_MADR_REGISTER_ADDRESS_Pos)

#define LPC_EMAC_MADR_PHY_ADDRESS_Pos            (8)
#define LPC_EMAC_MADR_PHY_ADDRESS_Msk            (0x1FUL << LPC_EMAC_MADR_PHY_ADDRESS_Pos)


#define LPC_EMAC_MIND_BUSY_Pos                   (0)
#define LPC_EMAC_MIND_BUSY_Msk                   (1UL << LPC_EMAC_MIND_BUSY_Pos)

#define LPC_EMAC_MIND_SCANNING_Pos               (1)
#define LPC_EMAC_MIND_SCANNING_Msk               (1UL << LPC_EMAC_MIND_SCANNING_Pos)

#define LPC_EMAC_MIND_NOT_VALID_Pos              (2)
#define LPC_EMAC_MIND_NOT_VALID_Msk              (1UL << LPC_EMAC_MIND_NOT_VALID_Pos)

#define LPC_EMAC_MIND_MIILinkFail_Pos            (3)
#define LPC_EMAC_MIND_MIILinkFail_Msk            (1UL << LPC_EMAC_MIILinkFail_Pos)


#define LPC_EMAC_Command_RxEnable_Pos            (0)
#define LPC_EMAC_Command_RxEnable_Msk            (1UL << LPC_EMAC_Command_RxEnable_Pos)

#define LPC_EMAC_Command_TxEnable_Pos            (1)
#define LPC_EMAC_Command_TxEnable_Msk            (1UL << LPC_EMAC_Command_TxEnable_Pos)

#define LPC_EMAC_Command_RegReset_Pos            (3)
#define LPC_EMAC_Command_RegReset_Msk            (1UL << LPC_EMAC_Command_RegReset_Pos)

#define LPC_EMAC_Command_TxReset_Pos             (4)
#define LPC_EMAC_Command_TxReset_Msk             (1UL << LPC_EMAC_Command_TxReset_Pos)

#define LPC_EMAC_Command_RxReset_Pos             (5)
#define LPC_EMAC_Command_RxReset_Msk             (1UL << LPC_EMAC_Command_RxReset_Pos)

#define LPC_EMAC_Command_PassRuntFrame_Pos       (6)
#define LPC_EMAC_Command_PassRuntFrame_Msk       (1UL << LPC_EMAC_Command_PassRuntFrame_Pos)

#define LPC_EMAC_Command_PassRxFilter_Pos        (7)
#define LPC_EMAC_Command_PassRxFilter_Msk        (1UL << LPC_EMAC_Command_PassRxFilter_Pos)

#define LPC_EMAC_Command_TxFlowControl_Pos       (8)
#define LPC_EMAC_Command_TxFlowControl_Msk       (1UL << LPC_EMAC_Command_TxFlowControl_Pos)

#define LPC_EMAC_Command_RMII_Pos                (9)
#define LPC_EMAC_Command_RMII_Msk                (1UL << LPC_EMAC_Command_RMII_Pos)

#define LPC_EMAC_Command_FullDuplex_Pos          (10)
#define LPC_EMAC_Command_FullDuplex_Msk          (1UL << LPC_EMAC_COmmand_FullDuplex_Pos)


#define LPC_EMAC_RxFilterCtrl_AcceptUnicastEn_Pos (0)
#define LPC_EMAC_RxFilterCtrl_AcceptUnicastEn_Msk (1UL << \
    LPC_EMAC_RxFilterCtrl_AcceptUnicastEn_Pos)

#define LPC_EMAC_RxFilterCtrl_AcceptBroadcastEn_Pos (1)
#define LPC_EMAC_RxFilterCtrl_AcceptBroadcastEn_Msk (1UL << \
    LPC_EMAC_RxFilterCtrl_AcceptBroadcastEn_Pos)

#define LPC_EMAC_RxFilterCtrl_AcceptMulticastEn_Pos (2)
#define LPC_EMAC_RxFilterCtrl_AcceptMulticastEn_Msk (1UL << \
    LPC_EMAC_RxFilterCtrl_AcceptMulticastEn_Pos)

#define LPC_EMAC_RxFilterCtrl_AcceptUnicastHashEn_Pos (3)
#define LPC_EMAC_RxFilterCtrl_AcceptUnicastHashEn_Msk (1UL << \
    LPC_EMAC_RxFilterCtrl_AcceptUnicastHashEn_Pos)

#define LPC_EMAC_RxFilterCtrl_AcceptMulticastHashEn_Pos (4)
#define LPC_EMAC_RxFilterCtrl_AcceptMulticastHashEn_Msk (1UL << \
    LPC_EMAC_RxFilterCtrl_AcceptMulticastHashEn_Pos)

#define LPC_EMAC_RxFilterCtrl_AcceptPerfectEn_Pos (5)
#define LPC_EMAC_RxFilterCtrl_AcceptPerfectEn_Msk (1UL << \
    LPC_EMAC_RxFilterCtrl_AcceptPerfectEn_Pos)

#define LPC_EMAC_RxFilterCtrl_MagicPacketEnWoL_Pos (12)
#define LPC_EMAC_RxFilterCtrl_MagicPacketEnWoL_Msk (1UL << \
    LPC_EMAC_RxFilterCtrl_MagicPacketEnWoL_Pos)

#define LPC_EMAC_RxFilterCtrl_RxFilterEnWoL_Pos  (13)
#define LPC_EMAC_RxFilterCtrl_RxFilterEnWoL_Msk  (1UL << LPC_EMAC_RxFilterCtrl_RxFilterEnWoL_Pos)


#define LPC_EMAC_IntStatus_RxOverrunInt_Pos (0)
#define LPC_EMAC_IntStatus_RxOverrunInt_Msk (1UL << LPC_EMAC_IntStatus_RxOverrunInt_Pos)

#define LPC_EMAC_IntStatus_RxErrorInt_Pos (1)
#define LPC_EMAC_IntStatus_RxErrorInt_Msk (1UL << LPC_EMAC_IntStatus_RxErrorInt_Pos)

#define LPC_EMAC_IntStatus_RxFinishedInt_Pos (2)
#define LPC_EMAC_IntStatus_RxFinishedInt_Msk (1UL << LPC_EMAC_IntStatus_RxFinishedInt_Pos)

#define LPC_EMAC_IntStatus_RxDoneInt_Pos (3)
#define LPC_EMAC_IntStatus_RxDoneInt_Msk (1UL << LPC_EMAC_IntStatus_RxDoneInt_Pos)

#define LPC_EMAC_IntStatus_TxUnderrunInt_Pos (4)
#define LPC_EMAC_IntStatus_TxUnderrunInt_Msk (1UL << LPC_EMAC_IntStatus_TxUnderrunInt_Pos)

#define LPC_EMAC_IntStatus_TxErrorInt_Pos (5)
#define LPC_EMAC_IntStatus_TxErrorInt_Msk (1UL << LPC_EMAC_IntStatus_TxErrorInt_Pos)

#define LPC_EMAC_IntStatus_TxFinishedInt_Pos (6)
#define LPC_EMAC_IntStatus_TxFinishedInt_Msk (1UL << LPC_EMAC_IntStatus_TxFinishedInt_Pos)

#define LPC_EMAC_IntStatus_TxDoneInt_Pos (7)
#define LPC_EMAC_IntStatus_TxDoneInt_Msk (1UL << LPC_EMAC_IntStatus_TxDoneInt_Pos)

#define LPC_EMAC_IntStatus_SoftInt_Pos (12)
#define LPC_EMAC_IntStatus_SoftInt_Msk (1UL << LPC_EMAC_IntStatus_SoftInt_Pos)

#define LPC_EMAC_IntStatus_WakeupInt_Pos (13)
#define LPC_EMAC_IntStatus_WakeupInt_Msk (1UL << LPC_EMAC_IntStatus_WakeupInt_Pos)


#define LPC_EMAC_IntEnable_RxOverrunIntEn_Pos (0)
#define LPC_EMAC_IntEnable_RxOverrunIntEn_Msk (1UL << LPC_EMAC_IntEnable_RxOverrunIntEn_Pos)

#define LPC_EMAC_IntEnable_RxErrorIntEn_Pos (1)
#define LPC_EMAC_IntEnable_RxErrorIntEn_Msk (1UL << LPC_EMAC_IntEnable_RxErrorIntEn_Pos)

#define LPC_EMAC_IntEnable_RxFinishedIntEn_Pos (2)
#define LPC_EMAC_IntEnable_RxFinishedIntEn_Msk (1UL << LPC_EMAC_IntEnable_RxFinishedIntEn_Pos)

#define LPC_EMAC_IntEnable_RxDoneIntEn_Pos (3)
#define LPC_EMAC_IntEnable_RxDoneIntEn_Msk (1UL << LPC_EMAC_IntEnable_RxDoneIntEn_Pos)

#define LPC_EMAC_IntEnable_TxUnderrunIntEn_Pos (4)
#define LPC_EMAC_IntEnable_TxUnderrunIntEn_Msk (1UL << LPC_EMAC_IntEnable_TxUnderrunIntEn_Pos)

#define LPC_EMAC_IntEnable_TxErrorIntEn_Pos (5)
#define LPC_EMAC_IntEnable_TxErrorIntEn_Msk (1UL << LPC_EMAC_IntEnable_TxErrorIntEn_Pos)

#define LPC_EMAC_IntEnable_TxFinishedIntEn_Pos (6)
#define LPC_EMAC_IntEnable_TxFinishedIntEn_Msk (1UL << LPC_EMAC_IntEnable_TxFinishedIntEn_Pos)

#define LPC_EMAC_IntEnable_TxDoneIntEn_Pos (7)
#define LPC_EMAC_IntEnable_TxDoneIntEn_Msk (1UL << LPC_EMAC_IntEnable_TxDoneIntEn_Pos)

#define LPC_EMAC_IntEnable_SoftIntEn_Pos (12)
#define LPC_EMAC_IntEnable_SoftIntEn_Msk (1UL << LPC_EMAC_IntEnable_SoftIntEn_Pos)

#define LPC_EMAC_IntEnable_WakeupIntEn_Pos (13)
#define LPC_EMAC_IntEnable_WakeupIntEn_Msk (1UL << LPC_EMAC_IntEnable_WakeupIntEn_Pos)


/*
 * UART
 */
#define LPC_UART_IER_RBRInterruptEnable_Pos      (0)
#define LPC_UART_IER_RBRInterruptEnable_Msk      (1UL << LPC_UART_IER_RBRInterruptEnable_Pos)

#define LPC_UART_IER_THREInterruptEnable_Pos     (1)
#define LPC_UART_IER_THREInterruptEnable_Msk     (1UL << LPC_UART_IER_THREInterruptEnable_Pos)

#define LPC_UART_IER_RXLineStatusInterruptEnable_Pos (2)
#define LPC_UART_IER_RXLineStatusInterruptEnable_Msk (1UL << \
                                                      LPC_UART_IER_RXLineStatusInterruptEnable_Pos)

#define LPC_UART_IER_ABEOIntEn_Pos               (8)
#define LPC_UART_IER_ABEOIntEn_Msk               (1UL << LPC_UART_IER_ABEOIntEn_Pos)

#define LPC_UART_IER_ABTOIntEn_Pos               (9)
#define LPC_UART_IER_ABTOIntEn_Msk               (1UL << LPC_UART_IER_ABTOIntEn_Pos)


#define LPC_UART_IIR_IntStatus_Pos               (0)
#define LPC_UART_IIR_IntStatus_Msk               (1UL << LPC_UART_IIR_IntStatus_Pos)

#define LPC_UART_IIR_IntId_Pos                   (1)
#define LPC_UART_IIR_IntId_Msk                   (0x07UL << LPC_UART_IIR_IntId_Pos)

#define LPC_UART_IIR_FIFOEnable_Pos              (6)
#define LPC_UART_IIR_FIFOEnable_Msk              (0x03UL << LPC_UART_IIR_FIFOEnable_Pos)

#define LPC_UART_IIR_ABEOInt_Pos                 (8)
#define LPC_UART_IIR_ABEOInt_Msk                 (1UL << LPC_UART_IIR_ABEOInt_Pos)

#define LPC_UART_IIR_ABTOInt_Pos                 (9)
#define LPC_UART_IIR_ABTOInt_Msk                 (1UL << LPC_UART_IIR_ABTOInt_Pos)


#define LPC_UART_FCR_FIFOEnable_Pos              (0)
#define LPC_UART_FCR_FIFOEnable_Msk              (1UL << LPC_UART_FCR_FIFOEnable_Pos)

#define LPC_UART_FCR_RXFIFOReset_Pos             (1)
#define LPC_UART_FCR_RXFIFOReset_Msk             (1UL << LPC_UART_FCR_RXFIFOReset_Pos)

#define LPC_UART_FCR_TXFIFOReset_Pos             (2)
#define LPC_UART_FCR_TXFIFOReset_Msk             (1UL << LPC_UART_FCR_TXFIFOReset_Pos)

#define LPC_UART_FCR_DMAModeSelect_Pos           (3)
#define LPC_UART_FCR_DMAModeSelect_Msk           (1UL << LPC_UART_FCR_DMAModeSelect_Pos)

#define LPC_UART_FCR_RXTriggerLevel_Pos          (6)
#define LPC_UART_FCR_RXTriggerLevel_Msk          (0x03UL << LPC_UART_FCR_RXTriggerLevel_Pos)


#define LPC_UART_LCR_WordLengthSelect_Pos        (0)
#define LPC_UART_LCR_WordLengthSelect_Msk        (0x03UL << LPC_UART_LCR_WordLengthSelect_Pos)

#define LPC_UART_LCR_StopBitSelect_Pos           (2)
#define LPC_UART_LCR_StopBitSelect_Msk           (1UL << LPC_UART_LCR_StopBitSelect_Pos)

#define LPC_UART_LCR_ParityEnable_Pos            (3)
#define LPC_UART_LCR_ParityEnable_Msk            (1UL << LPC_UART_LCR_ParityEnable_Pos)

#define LPC_UART_LCR_ParitySelect_Pos            (4)
#define LPC_UART_LCR_ParitySelect_Msk            (0x03UL << LPC_UART_LCR_ParitySelect_Pos)

#define LPC_UART_LCR_BreakControl_Pos            (6)
#define LPC_UART_LCR_BreakControl_Msk            (1UL << LPC_UART_LCR_BreakControl_Pos)

#define LPC_UART_LCR_DLAB_Pos                    (7)
#define LPC_UART_LCR_DLAB_Msk                    (1UL << LPC_UART_LCR_DLAB_Pos)


#define LPC_UART_LSR_RDR_Pos                     (0)
#define LPC_UART_LSR_RDR_Msk                     (1UL << LPC_UART_LSR_RDR_Pos)

#define LPC_UART_LSR_OE_Pos                      (1)
#define LPC_UART_LSR_OE_Msk                      (1UL << LPC_UART_LSR_OE_Pos)

#define LPC_UART_LSR_PE_Pos                      (2)
#define LPC_UART_LSR_PE_Msk                      (1UL << LPC_UART_LSR_PE_Pos)

#define LPC_UART_LSR_FE_Pos                      (3)
#define LPC_UART_LSR_FE_Msk                      (1UL << LPC_UART_LSR_FE_Pos)

#define LPC_UART_LSR_BI_Pos                      (4)
#define LPC_UART_LSR_BI_Msk                      (1UL << LPC_UART_LSR_BI_Pos)

#define LPC_UART_LSR_THRE_Pos                    (5)
#define LPC_UART_LSR_THRE_Msk                    (1UL << LPC_UART_LSR_THRE_Pos)

#define LPC_UART_LSR_TEMT_Pos                    (6)
#define LPC_UART_LSR_TEMT_Msk                    (1UL << LPC_UART_LSR_TEMT_Pos)

#define LPC_UART_LSR_RXFE_Pos                    (7)
#define LPC_UART_LSR_RXFE_Msk                    (1UL << LPC_UART_LSR_RXFE_Pos)


#define LPC_UART_FDR_DIVADDVAL_Pos               (0)
#define LPC_UART_FDR_DIVADDVAL_Msk               (0x0FUL << LPC_UART_FDR_DIVADDVAL_Pos)

#define LPC_UART_FDR_MULVAL_Pos                  (4)
#define LPC_UART_FDR_MULVAL_Msk                  (0x0FUL << LPC_UART_FDR_MULVAL_Pos)


/*
 * UART1
 */
#define LPC_UART1_RS485CTRL_NMMEN_Pos            (0)
#define LPC_UART1_RS485CTRL_NMMEN_Msk            (1UL << LPC_UART1_RS485CTRL_NMMEN_Pos)

#define LPC_UART1_RS485CTRL_RXDIS_Pos            (1)
#define LPC_UART1_RS485CTRL_RXDIS_Msk            (1UL << LPC_UART1_RS485CTRL_RXDIS_Pos)

#define LPC_UART1_RS485CTRL_AADEN_Pos            (2)
#define LPC_UART1_RS485CTRL_AADEN_Msk            (1UL << LPC_UART1_RS485CTRL_AADEN_Pos)

#define LPC_UART1_RS485CTRL_SEL_Pos              (3)
#define LPC_UART1_RS485CTRL_SEL_Msk              (1UL << LPC_UART1_RS485CTRL_SEL_Pos)

#define LPC_UART1_RS485CTRL_DCTRL_Pos            (4)
#define LPC_UART1_RS485CTRL_DCTRL_Msk            (1UL << LPC_UART1_RS485CTRL_DCTRL_Pos)

#define LPC_UART1_RS485CTRL_OINV_Pos             (5)
#define LPC_UART1_RS485CTRL_OINV_Msk             (1UL << LPC_UART1_RS485CTRL_OINV_Pos)


/*
 * SSP
 */
#define LPC_SSP_CR0_DSS_Pos                      (0)
#define LPC_SSP_CR0_DSS_Msk                      (0x0FUL << LPC_SSP_CR0_DSS_Pos)

#define LPC_SSP_CR0_FRF_Pos                      (4)
#define LPC_SSP_CR0_FRF_Msk                      (0x03UL << LPC_SSP_CR0_FRF_Pos)

#define LPC_SSP_CR0_CPOL_Pos                     (6)
#define LPC_SSP_CR0_CPOL_Msk                     (1UL << LPC_SSP_CR0_CPOL_Pos)

#define LPC_SSP_CR0_CPHA_Pos                     (7)
#define LPC_SSP_CR0_CPHA_Msk                     (1UL << LPC_SSP_CR0_CPHA_Pos)

#define LPC_SSP_CR0_SCR_Pos                      (8)
#define LPC_SSP_CR0_SCR_Msk                      (0xFFUL << LPC_SSP_CR0_SCR_Pos)


#define LPC_SSP_CR1_LBM_Pos                      (0)
#define LPC_SSP_CR1_LBM_Msk                      (1UL << LPC_SSP_CR1_LBM_Pos)

#define LPC_SSP_CR1_SSE_Pos                      (1)
#define LPC_SSP_CR1_SSE_Msk                      (1UL << LPC_SSP_CR1_SSE_Pos)

#define LPC_SSP_CR1_MS_Pos                       (2)
#define LPC_SSP_CR1_MS_Msk                       (1UL << LPC_SSP_CR1_MS_Pos)

#define LPC_SSP_CR1_SOD_Pos                      (3)
#define LPC_SSP_CR1_SOD_Msk                      (1UL << LPC_SSP_CR1_SOD_Pos)


#define LPC_SSP_SR_TFE_Pos                       (0)
#define LPC_SSP_SR_TFE_Msk                       (1UL << LPC_SSP_SR_TFE_Pos)

#define LPC_SSP_SR_TNF_Pos                       (1)
#define LPC_SSP_SR_TNF_Msk                       (1UL << LPC_SSP_SR_TNF_Pos)

#define LPC_SSP_SR_RNE_Pos                       (2)
#define LPC_SSP_SR_RNE_Msk                       (1UL << LPC_SSP_SR_RNE_Pos)

#define LPC_SSP_SR_RFF_Pos                       (3)
#define LPC_SSP_SR_RFF_Msk                       (1UL << LPC_SSP_SR_RFF_Pos)

#define LPC_SSP_SR_BSY_Pos                       (4)
#define LPC_SSP_SR_BSY_Msk                       (1UL << LPC_SSP_SR_BSY_Pos)


#define LPC_SSP_DMACR_RXDMAE_Pos                 (0)
#define LPC_SSP_DMACR_RXDMAE_Msk                 (1UL << LPC_SSP_DMACR_RXDMAE_Pos)

#define LPC_SSP_DMACR_TXDMAE_Pos                 (1)
#define LPC_SSP_DMACR_TXDMAE_Msk                 (1UL << LPC_SSP_DMACR_TXDMAE_Pos)


/*
 * Timer 0/1/2/3
 */
#define LPC_TIM_IR_MR0Interrupt_Pos              (0)
#define LPC_TIM_IR_MR0Interrupt_Msk              (1UL << LPC_TIM_IR_MR0Interrupt_Pos)

#define LPC_TIM_IR_MR1Interrupt_Pos              (1)
#define LPC_TIM_IR_MR1Interrupt_Msk              (1UL << LPC_TIM_IR_MR1Interrupt_Pos)

#define LPC_TIM_IR_MR2Interrupt_Pos              (2)
#define LPC_TIM_IR_MR2Interrupt_Msk              (1UL << LPC_TIM_IR_MR2Interrupt_Pos)

#define LPC_TIM_IR_MR3Interrupt_Pos              (3)
#define LPC_TIM_IR_MR3Interrupt_Msk              (1UL << LPC_TIM_IR_MR3Interrupt_Pos)

#define LPC_TIM_IR_CR0Interrupt_Pos              (4)
#define LPC_TIM_IR_CR0Interrupt_Msk              (1UL << LPC_TIM_IR_CR0Interrupt_Pos)

#define LPC_TIM_IR_CR1Interrupt_Pos              (5)
#define LPC_TIM_IR_CR1Interrupt_Msk              (1UL << LPC_TIM_IR_CR1Interrupt_Pos)


#define LPC_TIM_TCR_CounterEnable_Pos            (0)
#define LPC_TIM_TCR_CounterEnable_Msk            (1UL << LPC_TIM_TCR_CounterEnable_Pos)

#define LPC_TIM_TCR_CounterReset_Pos             (1)
#define LPC_TIM_TCR_CounterReset_Msk             (1UL << LPC_TIM_TCR_CounterReset_Pos)


#define LPC_TIM_MCR_MR0I_Pos                     (0)
#define LPC_TIM_MCR_MR0I_Msk                     (1UL << LPC_TIM_MCR_MR0I_Pos)

#define LPC_TIM_MCR_MR0R_Pos                     (1)
#define LPC_TIM_MCR_MR0R_Msk                     (1UL << LPC_TIM_MCR_MR0R_Pos)

#define LPC_TIM_MCR_MR0S_Pos                     (2)
#define LPC_TIM_MCR_MR0S_Msk                     (1UL << LPC_TIM_MCR_MR0S_Pos)

#define LPC_TIM_MCR_MR1I_Pos                     (3)
#define LPC_TIM_MCR_MR1I_Msk                     (1UL << LPC_TIM_MCR_MR1I_Pos)

#define LPC_TIM_MCR_MR1R_Pos                     (4)
#define LPC_TIM_MCR_MR1R_Msk                     (1UL << LPC_TIM_MCR_MR1R_Pos)

#define LPC_TIM_MCR_MR1S_Pos                     (5)
#define LPC_TIM_MCR_MR1S_Msk                     (1UL << LPC_TIM_MCR_MR1S_Pos)

#define LPC_TIM_MCR_MR2I_Pos                     (6)
#define LPC_TIM_MCR_MR2I_Msk                     (1UL << LPC_TIM_MCR_MR2I_Pos)

#define LPC_TIM_MCR_MR2R_Pos                     (7)
#define LPC_TIM_MCR_MR2R_Msk                     (1UL << LPC_TIM_MCR_MR2R_Pos)

#define LPC_TIM_MCR_MR2S_Pos                     (8)
#define LPC_TIM_MCR_MR2S_Msk                     (1UL << LPC_TIM_MCR_MR2S_Pos)

#define LPC_TIM_MCR_MR3I_Pos                     (9)
#define LPC_TIM_MCR_MR3I_Msk                     (1UL << LPC_TIM_MCR_MR3I_Pos)

#define LPC_TIM_MCR_MR3R_Pos                     (10)
#define LPC_TIM_MCR_MR3R_Msk                     (1UL << LPC_TIM_MCR_MR3R_Pos)

#define LPC_TIM_MCR_MR3S_Pos                     (11)
#define LPC_TIM_MCR_MR3S_Msk                     (1UL << LPC_TIM_MCR_MR3S_Pos)


#define LPC_TIM_CCR_CAP0RE_Pos                   (0)
#define LPC_TIM_CCR_CAP0RE_Msk                   (1UL << LPC_TIM_CCR_CAP0RE_Pos)

#define LPC_TIM_CCR_CAP0FE_Pos                   (1)
#define LPC_TIM_CCR_CAP0FE_Msk                   (1UL << LPC_TIM_CCR_CAP0FE_Pos)

#define LPC_TIM_CCR_CAP0I_Pos                    (2)
#define LPC_TIM_CCR_CAP0I_Msk                    (1UL << LPC_TIM_CCR_CAP0I_Pos)

#define LPC_TIM_CCR_CAP1RE_Pos                   (3)
#define LPC_TIM_CCR_CAP1RE_Msk                   (1UL << LPC_TIM_CCR_CAP1RE_Pos)

#define LPC_TIM_CCR_CAP1FE_Pos                   (4)
#define LPC_TIM_CCR_CAP1FE_Msk                   (1UL << LPC_TIM_CCR_CAP1FE_Pos)

#define LPC_TIM_CCR_CAP1I_Pos                    (5)
#define LPC_TIM_CCR_CAP1I_Msk                    (1UL << LPC_TIM_CCR_CAP1I_Pos)

#define LPC_TIM_EMR_EM0_Pos                     (0)
#define LPC_TIM_EMR_EM0_Msk                     (1UL << LPC_TIM_EMR_EM0_Pos)

#define LPC_TIM_EMR_EM1_Pos                     (1)
#define LPC_TIM_EMR_EM1_Msk                     (1UL << LPC_TIM_EMR_EM1_Pos)

#define LPC_TIM_EMR_EM2_Pos                     (2)
#define LPC_TIM_EMR_EM2_Msk                     (1UL << LPC_TIM_EMR_EM2_Pos)

#define LPC_TIM_EMR_EM3_Pos                     (3)
#define LPC_TIM_EMR_EM3_Msk                     (1UL << LPC_TIM_EMR_EM3_Pos)

#define LPC_TIM_EMR_EMC0_Pos                    (4)
#define LPC_TIM_EMR_EMC0_Msk                    (3UL << LPC_TIM_EMR_EMC0_Pos)

#define LPC_TIM_EMR_EMC1_Pos                    (6)
#define LPC_TIM_EMR_EMC1_Msk                    (3UL << LPC_TIM_EMR_EMC1_Pos)

#define LPC_TIM_EMR_EMC2_Pos                    (8)
#define LPC_TIM_EMR_EMC2_Msk                    (3UL << LPC_TIM_EMR_EMC2_Pos)

#define LPC_TIM_EMR_EMC3_Pos                    (10)
#define LPC_TIM_EMR_EMC3_Msk                    (3UL << LPC_TIM_EMR_EMC3_Pos)



/*
 * Repetitive Interrupt Timer
 */
#define LPC_RIT_RICTRL_RITINT_Pos                (0)
#define LPC_RIT_RICTRL_RITINT_Msk                (1UL << LPC_RIT_RICTRL_RITINT_Pos)

#define LPC_RIT_RICTRL_RITENCLR_Pos              (1)
#define LPC_RIT_RICTRL_RITENCLR_Msk              (1UL << LPC_RIT_RICTRL_RITENCLR_Pos)

#define LPC_RIT_RICTRL_RITENBR_Pos               (2)
#define LPC_RIT_RICTRL_RITENBR_Msk               (1UL << LPC_RIT_RICTRL_RITENBR_Pos)

#define LPC_RIT_RICTRL_RITEN_Pos                 (3)
#define LPC_RIT_RICTRL_RITEN_Msk                 (1UL << LPC_RIT_RICTRL_RITEN_Pos)


/*
 * PWM
 */
#define LPC_PWM_TCR_CounterEnable_Pos            (0)
#define LPC_PWM_TCR_CounterEnable_Msk            (1UL << LPC_PWM_TCR_CounterEnable_Pos)

#define LPC_PWM_TCR_CounterReset_Pos             (1)
#define LPC_PWM_TCR_CounterReset_Msk             (1UL << LPC_PWM_TCR_CounterReset_Pos)

#define LPC_PWM_TCR_PWMEnable_Pos                (3)
#define LPC_PWM_TCR_PWMEnable_Msk                (1UL << LPC_PWM_TCR_PWMEnable_Pos)


#define LPC_PWM_PCR_PWMSEL2_Pos                  (2)
#define LPC_PWM_PCR_PWMSEL2_Msk                  (1UL << LPC_PWM_PCR_PWMSEL2_Pos)

#define LPC_PWM_PCR_PWMSEL3_Pos                  (3)
#define LPC_PWM_PCR_PWMSEL3_Msk                  (1UL << LPC_PWM_PCR_PWMSEL3_Pos)

#define LPC_PWM_PCR_PWMSEL4_Pos                  (4)
#define LPC_PWM_PCR_PWMSEL4_Msk                  (1UL << LPC_PWM_PCR_PWMSEL4_Pos)

#define LPC_PWM_PCR_PWMSEL5_Pos                  (5)
#define LPC_PWM_PCR_PWMSEL5_Msk                  (1UL << LPC_PWM_PCR_PWMSEL5_Pos)

#define LPC_PWM_PCR_PWMSEL6_Pos                  (6)
#define LPC_PWM_PCR_PWMSEL6_Msk                  (1UL << LPC_PWM_PCR_PWMSEL6_Pos)

#define LPC_PWM_PCR_PWMENA1_Pos                  (9)
#define LPC_PWM_PCR_PWMENA1_Msk                  (1UL << LPC_PWM_PCR_PWMENA1_Pos)

#define LPC_PWM_PCR_PWMENA2_Pos                  (10)
#define LPC_PWM_PCR_PWMENA2_Msk                  (1UL << LPC_PWM_PCR_PWMENA2_Pos)

#define LPC_PWM_PCR_PWMENA3_Pos                  (11)
#define LPC_PWM_PCR_PWMENA3_Msk                  (1UL << LPC_PWM_PCR_PWMENA3_Pos)

#define LPC_PWM_PCR_PWMENA4_Pos                  (12)
#define LPC_PWM_PCR_PWMENA4_Msk                  (1UL << LPC_PWM_PCR_PWMENA4_Pos)

#define LPC_PWM_PCR_PWMENA5_Pos                  (13)
#define LPC_PWM_PCR_PWMENA5_Msk                  (1UL << LPC_PWM_PCR_PWMENA5_Pos)

#define LPC_PWM_PCR_PWMENA6_Pos                  (14)
#define LPC_PWM_PCR_PWMENA6_Msk                  (1UL << LPC_PWM_PCR_PWMENA6_Pos)


#define LPC_PWM_LER_EnablePWMMatch0Latch_Pos     (0)
#define LPC_PWM_LER_EnablePWMMatch0Latch_Msk     (1UL << LPC_PWM_LER_EnablePWMMatch0Latch_Pos)

#define LPC_PWM_LER_EnablePWMMatch1Latch_Pos     (1)
#define LPC_PWM_LER_EnablePWMMatch1Latch_Msk     (1UL << LPC_PWM_LER_EnablePWMMatch1Latch_Pos)

#define LPC_PWM_LER_EnablePWMMatch2Latch_Pos     (2)
#define LPC_PWM_LER_EnablePWMMatch2Latch_Msk     (1UL << LPC_PWM_LER_EnablePWMMatch2Latch_Pos)

#define LPC_PWM_LER_EnablePWMMatch3Latch_Pos     (3)
#define LPC_PWM_LER_EnablePWMMatch3Latch_Msk     (1UL << LPC_PWM_LER_EnablePWMMatch3Latch_Pos)

#define LPC_PWM_LER_EnablePWMMatch4Latch_Pos     (4)
#define LPC_PWM_LER_EnablePWMMatch4Latch_Msk     (1UL << LPC_PWM_LER_EnablePWMMatch4Latch_Pos)

#define LPC_PWM_LER_EnablePWMMatch5Latch_Pos     (5)
#define LPC_PWM_LER_EnablePWMMatch5Latch_Msk     (1UL << LPC_PWM_LER_EnablePWMMatch5Latch_Pos)

#define LPC_PWM_LER_EnablePWMMatch6Latch_Pos     (6)
#define LPC_PWM_LER_EnablePWMMatch6Latch_Msk     (1UL << LPC_PWM_LER_EnablePWMMatch6Latch_Pos)

#define LPC_PWM_IR_PWMMR0Interrupt_Pos           (0)
#define LPC_PWM_IR_PWMMR0Interrupt_Msk           (0x1UL << LPC_PWM_IR_PWMMR0Interrupt_Pos)

#define LPC_PWM_IR_PWMMR1Interrupt_Pos           (1)
#define LPC_PWM_IR_PWMMR1Interrupt_Msk           (0x1UL << LPC_PWM_IR_PWMMR1Interrupt_Pos)

#define LPC_PWM_IR_PWMMR2Interrupt_Pos           (2)
#define LPC_PWM_IR_PWMMR2Interrupt_Msk           (0x1UL << LPC_PWM_IR_PWMMR2Interrupt_Pos)

#define LPC_PWM_IR_PWMMR3Interrupt_Pos           (3)
#define LPC_PWM_IR_PWMMR3Interrupt_Msk           (0x1UL << LPC_PWM_IR_PWMMR3Interrupt_Pos)

#define LPC_PWM_IR_PWMCAP0Interrupt_Pos          (4)
#define LPC_PWM_IR_PWMCAP0Interrupt_Msk          (0x1UL << LPC_PWM_IR_PWMCAP0Interrupt_Pos)

#define LPC_PWM_IR_PWMCAP1Interrupt_Pos          (5)
#define LPC_PWM_IR_PWMCAP1Interrupt_Msk          (0x1UL << LPC_PWM_IR_PWMCAP1Interrupt_Pos)

#define LPC_PWM_IR_PWMMR4Interrupt_Pos           (8)
#define LPC_PWM_IR_PWMMR4Interrupt_Msk           (0x1UL << LPC_PWM_IR_PWMMR4Interrupt_Pos)

#define LPC_PWM_IR_PWMMR5Interrupt_Pos           (9)
#define LPC_PWM_IR_PWMMR5Interrupt_Msk           (0x1UL << LPC_PWM_IR_PWMMR5Interrupt_Pos)

#define LPC_PWM_IR_PWMMR6Interrupt_Pos           (10)
#define LPC_PWM_IR_PWMMR6Interrupt_Msk           (0x1UL << LPC_PWM_IR_PWMMR6Interrupt_Pos)


#define LPC_PWM_MCR_PWMMR0I_Pos                  (0)
#define LPC_PWM_MCR_PWMMR0I_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR0I_Pos)

#define LPC_PWM_MCR_PWMMR0R_Pos                  (1)
#define LPC_PWM_MCR_PWMMR0R_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR0R_Pos)

#define LPC_PWM_MCR_PWMMR0S_Pos                  (2)
#define LPC_PWM_MCR_PWMMR0S_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR0S_Pos)

#define LPC_PWM_MCR_PWMMR1I_Pos                  (3)
#define LPC_PWM_MCR_PWMMR1I_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR1I_Pos)

#define LPC_PWM_MCR_PWMMR1R_Pos                  (4)
#define LPC_PWM_MCR_PWMMR1R_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR1R_Pos)

#define LPC_PWM_MCR_PWMMR1S_Pos                  (5)
#define LPC_PWM_MCR_PWMMR1S_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR1S_Pos)

#define LPC_PWM_MCR_PWMMR2I_Pos                  (6)
#define LPC_PWM_MCR_PWMMR2I_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR2I_Pos)

#define LPC_PWM_MCR_PWMMR2R_Pos                  (7)
#define LPC_PWM_MCR_PWMMR2R_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR2R_Pos)

#define LPC_PWM_MCR_PWMMR2S_Pos                  (8)
#define LPC_PWM_MCR_PWMMR2S_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR2S_Pos)

#define LPC_PWM_MCR_PWMMR3I_Pos                  (9)
#define LPC_PWM_MCR_PWMMR3I_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR3I_Pos)

#define LPC_PWM_MCR_PWMMR3R_Pos                  (10)
#define LPC_PWM_MCR_PWMMR3R_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR3R_Pos)

#define LPC_PWM_MCR_PWMMR3S_Pos                  (11)
#define LPC_PWM_MCR_PWMMR3S_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR3S_Pos)

#define LPC_PWM_MCR_PWMMR4I_Pos                  (12)
#define LPC_PWM_MCR_PWMMR4I_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR4I_Pos)

#define LPC_PWM_MCR_PWMMR4R_Pos                  (13)
#define LPC_PWM_MCR_PWMMR4R_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR4R_Pos)

#define LPC_PWM_MCR_PWMMR4S_Pos                  (14)
#define LPC_PWM_MCR_PWMMR4S_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR4S_Pos)

#define LPC_PWM_MCR_PWMMR5I_Pos                  (15)
#define LPC_PWM_MCR_PWMMR5I_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR5I_Pos)

#define LPC_PWM_MCR_PWMMR5R_Pos                  (16)
#define LPC_PWM_MCR_PWMMR5R_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR5R_Pos)

#define LPC_PWM_MCR_PWMMR5S_Pos                  (17)
#define LPC_PWM_MCR_PWMMR5S_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR5S_Pos)

#define LPC_PWM_MCR_PWMMR6I_Pos                  (18)
#define LPC_PWM_MCR_PWMMR6I_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR6I_Pos)

#define LPC_PWM_MCR_PWMMR6R_Pos                  (19)
#define LPC_PWM_MCR_PWMMR6R_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR6R_Pos)

#define LPC_PWM_MCR_PWMMR6S_Pos                  (20)
#define LPC_PWM_MCR_PWMMR6S_Msk                  (0x1UL << LPC_PWM_MCR_PWMMR6S_Pos)



/*
 * RTC
 */
#define LPC_RTC_ILR_RTCCIF_Pos                   (0)
#define LPC_RTC_ILR_RTCCIF_Msk                   (1UL << LPC_RTC_ILR_RTCCIF_Pos)

#define LPC_RTC_ILR_RTCALF_Pos                   (1)
#define LPC_RTC_ILR_RTCALF_Msk                   (1UL << LPC_RTC_ILR_RTCALF_Pos)


#define LPC_RTC_CCR_CLKEN_Pos                    (0)
#define LPC_RTC_CCR_CLKEN_Msk                    (1UL << LPC_RTC_CCR_CLKEN_Pos)

#define LPC_RTC_CCR_CTCRST_Pos                   (1)
#define LPC_RTC_CCR_CTCRST_Msk                   (1UL << LPC_RTC_CCR_CTCRST_Pos)

#define LPC_RTC_CCR_CCALEN_Pos                   (4)
#define LPC_RTC_CCR_CCALEN_Msk                   (1UL << LPC_RTC_CCR_CCALEN_Pos)


#define LPC_RTC_CIIR_IMSEC_Pos                   (0)
#define LPC_RTC_CIIR_IMSEC_Msk                   (1UL << LPC_RTC_CIIR_IMSEC_Pos)

#define LPC_RTC_CIIR_IMMIN_Pos                   (1)
#define LPC_RTC_CIIR_IMMIN_Msk                   (1UL << LPC_RTC_CIIR_IMMIN_Pos)

#define LPC_RTC_CIIR_IMHOUR_Pos                  (2)
#define LPC_RTC_CIIR_IMHOUR_Msk                  (1UL << LPC_RTC_CIIR_IMHOUR_Pos)

#define LPC_RTC_CIIR_IMDOM_Pos                   (3)
#define LPC_RTC_CIIR_IMDOM_Msk                   (1UL << LPC_RTC_CIIR_IMDOM_Pos)

#define LPC_RTC_CIIR_IMDOW_Pos                   (4)
#define LPC_RTC_CIIR_IMDOW_Msk                   (1UL << LPC_RTC_CIIR_IMDOW_Pos)

#define LPC_RTC_CIIR_IMDOY_Pos                   (5)
#define LPC_RTC_CIIR_IMDOY_Msk                   (1UL << LPC_RTC_CIIR_IMDOY_Pos)

#define LPC_RTC_CIIR_IMMON_Pos                   (6)
#define LPC_RTC_CIIR_IMMON_Msk                   (1UL << LPC_RTC_CIIR_IMMON_Pos)

#define LPC_RTC_CIIR_IMYEAR_Pos                  (7)
#define LPC_RTC_CIIR_IMYEAR_Msk                  (1UL << LPC_RTC_CIIR_IMYEAR_Pos)


#define LPC_RTC_AMR_AMRSEC_Pos                   (0)
#define LPC_RTC_AMR_AMRSEC_Msk                   (1UL << LPC_RTC_AMR_AMRSEC_Pos)

#define LPC_RTC_AMR_AMRMIN_Pos                   (1)
#define LPC_RTC_AMR_AMRMIN_Msk                   (1UL << LPC_RTC_AMR_AMRMIN_Pos)

#define LPC_RTC_AMR_AMRHOUR_Pos                  (2)
#define LPC_RTC_AMR_AMRHOUR_Msk                  (1UL << LPC_RTC_AMR_AMRHOUR_Pos)

#define LPC_RTC_AMR_AMRDOM_Pos                   (3)
#define LPC_RTC_AMR_AMRDOM_Msk                   (1UL << LPC_RTC_AMR_AMRDOM_Pos)

#define LPC_RTC_AMR_AMRDOW_Pos                   (4)
#define LPC_RTC_AMR_AMRDOW_Msk                   (1UL << LPC_RTC_AMR_AMRDOW_Pos)

#define LPC_RTC_AMR_AMRDOY_Pos                   (5)
#define LPC_RTC_AMR_AMRDOY_Msk                   (1UL << LPC_RTC_AMR_AMRDOY_Pos)

#define LPC_RTC_AMR_AMRMON_Pos                   (6)
#define LPC_RTC_AMR_AMRMON_Msk                   (1UL << LPC_RTC_AMR_AMRMON_Pos)

#define LPC_RTC_AMR_AMRYEAR_Pos                  (7)
#define LPC_RTC_AMR_AMRYEAR_Msk                  (1UL << LPC_RTC_AMR_AMRYEAR_Pos)


#define LPC_RTC_CTIME0_Seconds_Pos               (0)
#define LPC_RTC_CTIME0_Seconds_Msk               (0x3FUL << LPC_RTC_CTIME0_Seconds_Pos)

#define LPC_RTC_CTIME0_Minutes_Pos               (8)
#define LPC_RTC_CTIME0_Minutes_Msk               (0x3FUL << LPC_RTC_CTIME0_Minutes_Pos)

#define LPC_RTC_CTIME0_Hours_Pos                 (16)
#define LPC_RTC_CTIME0_Hours_Msk                 (0x1FUL << LPC_RTC_CTIME0_Hours_Pos)

#define LPC_RTC_CTIME0_DayOfWeek_Pos             (24)
#define LPC_RTC_CTIME0_DayOfWeek_Msk             (0x07UL << LPC_RTC_CTIME0_DayOfWeek_Pos)


#define LPC_RTC_CTIME1_DayofMonth_Pos            (0)
#define LPC_RTC_CTIME1_DayofMonth_Msk            (0x1FUL << LPC_RTC_CTIME1_DayofMonth_Pos)

#define LPC_RTC_CTIME1_Month_Pos                 (8)
#define LPC_RTC_CTIME1_Month_Msk                 (0x0FUL << LPC_RTC_CTIME1_Month_Pos)

#define LPC_RTC_CTIME1_Year_Pos                  (16)
#define LPC_RTC_CTIME1_Year_Msk                  (0x0FFFUL << LPC_RTC_CTIME1_Year_Pos)

#define LPC_RTC_CTIME2_DayOfYear_Pos             (0)
#define LPC_RTC_CTIME2_DayOfYear_Msk             (0x0FFFUL << LPC_RTC_CTIME2_DayOfYear_Pos)


#define LPC_RTC_AUX_RTC_OSCF_Pos                 (4)
#define LPC_RTC_AUX_RTC_OSCF_Msk                 (1UL << LPC_RTC_AUX_RTC_OSCF_Pos)


#define LPC_RTC_AUXEN_RTC_OSCFEN_Pos             (4)
#define LPC_RTC_AUXEN_RTC_OSCFEN_Msk             (1UL << LPC_RTC_AUXEN_RTC_OSCFEN_Pos)

/*
 * ADC
 */
#define LPC_ADC_ADCR_SEL_Pos                     (0)
#define LPC_ADC_ADCR_SEL_Msk                     (0xFFUL << LPC_ADC_ADCR_SEL_Pos)

#define LPC_ADC_ADCR_CLKDIV_Pos                  (8)
#define LPC_ADC_ADCR_CLKDIV_Msk                  (0xFFUL << LPC_ADC_ADCR_CLKDIV_Pos)

#define LPC_ADC_ADCR_BURST_Pos                   (16)
#define LPC_ADC_ADCR_BURST_Msk                   (1UL << LPC_ADC_ADCR_BURST_Pos)

#define LPC_ADC_ADCR_PDN_Pos                     (21)
#define LPC_ADC_ADCR_PDN_Msk                     (1UL << LPC_ADC_ADCR_PDN_Pos)

#define LPC_ADC_ADCR_START_Pos                   (24)
#define LPC_ADC_ADCR_START_Msk                   (0x07UL << LPC_ADC_ADCR_START_Pos)

#define LPC_ADC_ADCR_EDGE_Pos                    (27)
#define LPC_ADC_ADCR_EDGE_Msk                    (1UL << LPC_ADC_ADCR_EDGE_Pos)


#define LPC_ADC_ADGDR_RESULT_Pos                 (4)
#define LPC_ADC_ADGDR_RESULT_Msk                 (0x0FFFUL << LPC_ADC_ADGDR_RESULT_Pos)

#define LPC_ADC_ADGDR_CHN_Pos                    (24)
#define LPC_ADC_ADGDR_CHN_Msk                    (0x07UL << LPC_ADC_ADGDR_CHN_Pos)

#define LPC_ADC_ADGDR_OVERRUN_Pos                (30)
#define LPC_ADC_ADGDR_OVERRUN_Msk                (1UL << LPC_ADC_ADGDR_OVERRUN_Pos)

#define LPC_ADC_ADGDR_DONE_Pos                   (31)
#define LPC_ADC_ADGDR_DONE_Msk                   (1UL << LPC_ADC_ADGDR_DONE_Pos)


#define LPC_ADC_ADINTEN_ADINTEN0_Pos             (0)
#define LPC_ADC_ADINTEN_ADINTEN0_Msk             (1UL << LPC_ADC_ADINTEN_ADINTEN0_Pos)

#define LPC_ADC_ADINTEN_ADINTEN1_Pos             (1)
#define LPC_ADC_ADINTEN_ADINTEN1_Msk             (1UL << LPC_ADC_ADINTEN_ADINTEN1_Pos)

#define LPC_ADC_ADINTEN_ADINTEN2_Pos             (2)
#define LPC_ADC_ADINTEN_ADINTEN2_Msk             (1UL << LPC_ADC_ADINTEN_ADINTEN2_Pos)

#define LPC_ADC_ADINTEN_ADINTEN3_Pos             (3)
#define LPC_ADC_ADINTEN_ADINTEN3_Msk             (1UL << LPC_ADC_ADINTEN_ADINTEN3_Pos)

#define LPC_ADC_ADINTEN_ADINTEN4_Pos             (4)
#define LPC_ADC_ADINTEN_ADINTEN4_Msk             (1UL << LPC_ADC_ADINTEN_ADINTEN4_Pos)

#define LPC_ADC_ADINTEN_ADINTEN5_Pos             (5)
#define LPC_ADC_ADINTEN_ADINTEN5_Msk             (1UL << LPC_ADC_ADINTEN_ADINTEN5_Pos)

#define LPC_ADC_ADINTEN_ADINTEN6_Pos             (6)
#define LPC_ADC_ADINTEN_ADINTEN6_Msk             (1UL << LPC_ADC_ADINTEN_ADINTEN6_Pos)

#define LPC_ADC_ADINTEN_ADINTEN7_Pos             (7)
#define LPC_ADC_ADINTEN_ADINTEN7_Msk             (1UL << LPC_ADC_ADINTEN_ADINTEN7_Pos)

#define LPC_ADC_ADINTEN_ADGINTEN_Pos             (8)
#define LPC_ADC_ADINTEN_ADGINTEN_Msk             (1UL << LPC_ADC_ADINTEN_ADGINTEN_Pos)


#define LPC_ADC_ADDR_RESULT_Pos                  (4)
#define LPC_ADC_ADDR_RESULT_Msk                  (0x0FFFUL << LPC_ADC_ADDR_RESULT_Pos)

#define LPC_ADC_ADDR_OVERRUN_Pos                 (30)
#define LPC_ADC_ADDR_OVERRUN_Msk                 (0x01UL << LPC_ADC_ADDR_OVERRUN_Pos)

#define LPC_ADC_ADDR_DONE_Pos                    (31)
#define LPC_ADC_ADDR_DONE_Msk                    (0x01UL << LPC_ADC_ADDR_DONE_Pos)


#define LPC_ADC_ADSTAT_DONE0_Pos                 (0)
#define LPC_ADC_ADSTAT_DONE0_Msk                 (0x01UL << LPC_ADC_ADSTAT_DONE0_Pos)

#define LPC_ADC_ADSTAT_DONE1_Pos                 (1)
#define LPC_ADC_ADSTAT_DONE1_Msk                 (0x01UL << LPC_ADC_ADSTAT_DONE1_Pos)

#define LPC_ADC_ADSTAT_DONE2_Pos                 (2)
#define LPC_ADC_ADSTAT_DONE2_Msk                 (0x01UL << LPC_ADC_ADSTAT_DONE2_Pos)

#define LPC_ADC_ADSTAT_DONE3_Pos                 (3)
#define LPC_ADC_ADSTAT_DONE3_Msk                 (0x01UL << LPC_ADC_ADSTAT_DONE3_Pos)

#define LPC_ADC_ADSTAT_DONE4_Pos                 (4)
#define LPC_ADC_ADSTAT_DONE4_Msk                 (0x01UL << LPC_ADC_ADSTAT_DONE4_Pos)

#define LPC_ADC_ADSTAT_DONE5_Pos                 (5)
#define LPC_ADC_ADSTAT_DONE5_Msk                 (0x01UL << LPC_ADC_ADSTAT_DONE5_Pos)

#define LPC_ADC_ADSTAT_DONE6_Pos                 (6)
#define LPC_ADC_ADSTAT_DONE6_Msk                 (0x01UL << LPC_ADC_ADSTAT_DONE6_Pos)

#define LPC_ADC_ADSTAT_DONE7_Pos                 (7)
#define LPC_ADC_ADSTAT_DONE7_Msk                 (0x01UL << LPC_ADC_ADSTAT_DONE7_Pos)

#define LPC_ADC_ADSTAT_OVERRUN0_Pos              (8)
#define LPC_ADC_ADSTAT_OVERRUN0_Msk              (0x01UL << LPC_ADC_ADSTAT_OVERRUN0_Pos)

#define LPC_ADC_ADSTAT_OVERRUN1_Pos              (9)
#define LPC_ADC_ADSTAT_OVERRUN1_Msk              (0x01UL << LPC_ADC_ADSTAT_OVERRUN1_Pos)

#define LPC_ADC_ADSTAT_OVERRUN2_Pos              (10)
#define LPC_ADC_ADSTAT_OVERRUN2_Msk              (0x01UL << LPC_ADC_ADSTAT_OVERRUN2_Pos)

#define LPC_ADC_ADSTAT_OVERRUN3_Pos              (11)
#define LPC_ADC_ADSTAT_OVERRUN3_Msk              (0x01UL << LPC_ADC_ADSTAT_OVERRUN3_Pos)

#define LPC_ADC_ADSTAT_OVERRUN4_Pos              (12)
#define LPC_ADC_ADSTAT_OVERRUN4_Msk              (0x01UL << LPC_ADC_ADSTAT_OVERRUN4_Pos)

#define LPC_ADC_ADSTAT_OVERRUN5_Pos              (13)
#define LPC_ADC_ADSTAT_OVERRUN5_Msk              (0x01UL << LPC_ADC_ADSTAT_OVERRUN5_Pos)

#define LPC_ADC_ADSTAT_OVERRUN6_Pos              (14)
#define LPC_ADC_ADSTAT_OVERRUN6_Msk              (0x01UL << LPC_ADC_ADSTAT_OVERRUN6_Pos)

#define LPC_ADC_ADSTAT_OVERRUN7_Pos              (15)
#define LPC_ADC_ADSTAT_OVERRUN7_Msk              (0x01UL << LPC_ADC_ADSTAT_OVERRUN7_Pos)

#define LPC_ADC_ADSTAT_ADINT_Pos                 (16)
#define LPC_ADC_ADSTAT_ADINT_Msk                 (0x01UL << LPC_ADC_ADSTAT_ADINT_Pos)


#define LPC_ADC_ADTRIM_ADCOFFS_Pos               (4)
#define LPC_ADC_ADTRIM_ADCOFFS_Msk               (0x0FUL << LPC_ADC_ADTRIM_ADCOFFS_Pos)

#define LPC_ADC_ADTRIM_TRIM_Pos                  (8)
#define LPC_ADC_ADTRIM_TRIM_Msk                  (0x0FUL << LPC_ADC_ADTRIM_TRIM_Pos)



/*
 * GPDMA
 */
#define LPC_GPDMA_DMACIntStat_IntStat_Pos        (0)
#define LPC_GPDMA_DMACIntStat_IntStat_Msk        (0xFFUL << LPC_GPDMA_DMACIntStat_IntStat_Pos)


#define LPC_GPDMA_DMACIntTCClear_IntTCClear_Pos  (0)
#define LPC_GPDMA_DMACIntTCClear_IntTCClear_Msk  (0xFFUL << LPC_GPDMA_DMACIntTCClear_IntTCClear_Pos)


#define LPC_GPDMA_DMACConfig_E_Pos               (0)
#define LPC_GPDMA_DMACConfig_E_Msk               (1UL << LPC_GPDMA_DMACConfig_E_Pos)

#define LPC_GPDMA_DMACConfig_M_Pos               (1)
#define LPC_GPDMA_DMACConfig_M_Msk               (1UL << LPC_GPDMA_DMACConfig_M_Pos)


#define LPC_GPDMA_DMAReqSel_DMASEL08_Pos         (0)
#define LPC_GPDMA_DMAReqSel_DMASEL08_Msk         (1UL << LPC_GPDMA_DMAReqSel_DMASEL08_Pos)

#define LPC_GPDMA_DMAReqSel_DMASEL09_Pos         (1)
#define LPC_GPDMA_DMAReqSel_DMASEL09_Msk         (1UL << LPC_GPDMA_DMAReqSel_DMASEL09_Pos)

#define LPC_GPDMA_DMAReqSel_DMASEL10_Pos         (2)
#define LPC_GPDMA_DMAReqSel_DMASEL10_Msk         (1UL << LPC_GPDMA_DMAReqSel_DMASEL10_Pos)

#define LPC_GPDMA_DMAReqSel_DMASEL11_Pos         (3)
#define LPC_GPDMA_DMAReqSel_DMASEL11_Msk         (1UL << LPC_GPDMA_DMAReqSel_DMASEL11_Pos)

#define LPC_GPDMA_DMAReqSel_DMASEL12_Pos         (4)
#define LPC_GPDMA_DMAReqSel_DMASEL12_Msk         (1UL << LPC_GPDMA_DMAReqSel_DMASEL12_Pos)

#define LPC_GPDMA_DMAReqSel_DMASEL13_Pos         (5)
#define LPC_GPDMA_DMAReqSel_DMASEL13_Msk         (1UL << LPC_GPDMA_DMAReqSel_DMASEL13_Pos)

#define LPC_GPDMA_DMAReqSel_DMASEL14_Pos         (6)
#define LPC_GPDMA_DMAReqSel_DMASEL14_Msk         (1UL << LPC_GPDMA_DMAReqSel_DMASEL14_Pos)

#define LPC_GPDMA_DMAReqSel_DMASEL15_Pos         (7)
#define LPC_GPDMA_DMAReqSel_DMASEL15_Msk         (1UL << LPC_GPDMA_DMAReqSel_DMASEL15_Pos)


#define LPC_GPDMACH_DMACCControl_TransferSize_Pos (0)
#define LPC_GPDMACH_DMACCControl_TransferSize_Msk (0x0FFFUL << LPC_GPDMACH_DMACCControl_TransferSize_Pos)

#define LPC_GPDMACH_DMACCControl_SBSize_Pos      (12)
#define LPC_GPDMACH_DMACCControl_SBSize_Msk      (0x07UL << LPC_GPDMACH_DMACCControl_SBSize_Pos)

#define LPC_GPDMACH_DMACCControl_DBSize_Pos      (15)
#define LPC_GPDMACH_DMACCControl_DBSize_Msk      (0x07UL << LPC_GPDMACH_DMACCControl_DBSize_Pos)

#define LPC_GPDMACH_DMACCControl_SWidth_Pos      (18)
#define LPC_GPDMACH_DMACCControl_SWidth_Msk      (0x07UL << LPC_GPDMACH_DMACCControl_SWidth_Pos)

#define LPC_GPDMACH_DMACCControl_DWidth_Pos      (21)
#define LPC_GPDMACH_DMACCControl_DWidth_Msk      (0x07UL << LPC_GPDMACH_DMACCControl_DWidth_Pos)

#define LPC_GPDMACH_DMACCControl_SI_Pos          (26)
#define LPC_GPDMACH_DMACCControl_SI_Msk          (1UL << LPC_GPDMACH_DMACCControl_SI_Pos)

#define LPC_GPDMACH_DMACCControl_DI_Pos          (27)
#define LPC_GPDMACH_DMACCControl_DI_Msk          (1UL << LPC_GPDMACH_DMACCControl_DI_Pos)

#define LPC_GPDMACH_DMACCControl_Prot1_Pos       (28)
#define LPC_GPDMACH_DMACCControl_Prot1_Msk       (1UL << LPC_GPDMACH_DMACCControl_Prot1_Pos)

#define LPC_GPDMACH_DMACCControl_Prot2_Pos       (29)
#define LPC_GPDMACH_DMACCControl_Prot2_Msk       (1UL << LPC_GPDMACH_DMACCControl_Prot2_Pos)

#define LPC_GPDMACH_DMACCControl_Prot3_Pos       (30)
#define LPC_GPDMACH_DMACCControl_Prot3_Msk       (1UL << LPC_GPDMACH_DMACCControl_Prot3_Pos)

#define LPC_GPDMACH_DMACCControl_I_Pos           (31)
#define LPC_GPDMACH_DMACCControl_I_Msk           (1UL << LPC_GPDMACH_DMACCControl_I_Pos)


#define LPC_GPDMACH_DMACCConfig_E_Pos            (0)
#define LPC_GPDMACH_DMACCConfig_E_Msk            (1UL << LPC_GPDMACH_DMACCConfig_E_Pos)

#define LPC_GPDMACH_DMACCConfig_SrcPeripheral_Pos (1)
#define LPC_GPDMACH_DMACCConfig_SrcPeripheral_Msk (0x1FUL << LPC_GPDMACH_DMACCConfig_SrcPeripheral_Pos)

#define LPC_GPDMACH_DMACCConfig_DestPeripheral_Pos (6)
#define LPC_GPDMACH_DMACCConfig_DestPeripheral_Msk (0x1F << LPC_GPDMACH_DMACCConfig_DestPeripheral_Pos)

#define LPC_GPDMACH_DMACCConfig_TransferType_Pos (11)
#define LPC_GPDMACH_DMACCConfig_TransferType_Msk (0x07UL << LPC_GPDMACH_DMACCConfig_TransferType_Pos)

#define LPC_GPDMACH_DMACCConfig_IE_Pos           (14)
#define LPC_GPDMACH_DMACCConfig_IE_Msk           (1UL << LPC_GPDMACH_DMACCConfig_IE_Pos)

#define LPC_GPDMACH_DMACCConfig_ITC_Pos          (15)
#define LPC_GPDMACH_DMACCConfig_ITC_Msk          (1UL << LPC_GPDMACH_DMACCConfig_ITC_Pos)

#define LPC_GPDMACH_DMACCConfig_L_Pos            (16)
#define LPC_GPDMACH_DMACCConfig_L_Msk            (1UL << LPC_GPDMACH_DMACCConfig_L_Pos)

#define LPC_GPDMACH_DMACCConfig_A_Pos            (17)
#define LPC_GPDMACH_DMACCConfig_A_Msk            (1UL << LPC_GPDMACH_DMACCConfig_A_Pos)

#define LPC_GPDMACH_DMACCConfig_H_Pos            (18)
#define LPC_GPDMACH_DMACCConfig_H_Msk            (1UL << LPC_GPDMACH_DMACCConfig_H_Pos)


/*
 *  DAC
 */
#define LPC_DAC_DACR_VALUE_Pos               (6)
#define LPC_DAC_DACR_VALUE_Msk               (0x3FFUL << LPC_DAC_DACR_VALUE_Pos)

#define LPC_DAC_DACR_BIAS_Pos                (16)
#define LPC_DAC_DACR_BIAS_Msk                (1UL << LPC_DAC_DACR_BIAS_Pos)


/*
 *  CAN
 */
/* CAN Mode register */
#define LPC_CAN_MOD_RM_Pos                   (0)
#define LPC_CAN_MOD_RM_Msk                   (1UL << LPC_CAN_MOD_RM_Pos)

#define LPC_CAN_MOD_LOM_Pos                  (1)
#define LPC_CAN_MOD_LOM_Msk                  (1UL << LPC_CAN_MOD_LOM_Pos)

#define LPC_CAN_MOD_STM_Pos                  (2)
#define LPC_CAN_MOD_STM_Msk                  (1UL << LPC_CAN_MOD_STM_Pos)

#define LPC_CAN_MOD_TPM_Pos                  (3)
#define LPC_CAN_MOD_TPM_Msk                  (1UL << LPC_CAN_MOD_TPM_Pos)

#define LPC_CAN_MOD_SM_Pos                   (4)
#define LPC_CAN_MOD_SM_Msk                   (1UL << LPC_CAN_MOD_SM_Pos)

#define LPC_CAN_MOD_RPM_Pos                  (5)
#define LPC_CAN_MOD_RPM_Msk                  (1UL << LPC_CAN_MOD_RPM_Pos)

#define LPC_CAN_MOD_TM_Pos                   (7)
#define LPC_CAN_MOD_TM_Msk                   (1UL << LPC_CAN_MOD_TM_Pos)

/* CAN Command Register */
#define LPC_CAN_CMR_TR_Pos                   (0)
#define LPC_CAN_CMR_TR_Msk                   (1UL << LPC_CAN_CMR_TR_Pos)

#define LPC_CAN_CMR_AT_Pos                   (1)
#define LPC_CAN_CMR_AT_Msk                   (1UL << LPC_CAN_CMR_AT_Pos)

#define LPC_CAN_CMR_RRB_Pos                  (2)
#define LPC_CAN_CMR_RRB_Msk                  (1UL << LPC_CAN_CMR_RRB_Pos)

#define LPC_CAN_CMR_CDO_Pos                  (3)
#define LPC_CAN_CMR_CDO_Msk                  (1UL << LPC_CAN_CMR_CDO_Pos)

#define LPC_CAN_CMR_SRR_Pos                  (4)
#define LPC_CAN_CMR_SRR_Msk                  (1UL << LPC_CAN_CMR_SRR_Pos)

#define LPC_CAN_CMR_STB1_Pos                 (5)
#define LPC_CAN_CMR_STB1_Msk                 (1UL << LPC_CAN_CMR_STB1_Pos)

#define LPC_CAN_CMR_STB2_Pos                 (6)
#define LPC_CAN_CMR_STB2_Msk                 (1UL << LPC_CAN_CMR_STB2_Pos)

#define LPC_CAN_CMR_STB3_Pos                 (7)
#define LPC_CAN_CMR_STB3_Msk                 (1UL << LPC_CAN_CMR_STB3_Pos)

/* CAN Global Status Register*/
#define LPC_CAN_GSR_RBS_Pos                  (0)
#define LPC_CAN_GSR_RBS_Msk                  (1UL << LPC_CAN_GSR_RBS_Pos)

#define LPC_CAN_GSR_DOS_Pos                  (1)
#define LPC_CAN_GSR_DOS_Msk                  (1UL << LPC_CAN_GSR_DOS_Pos)

#define LPC_CAN_GSR_TBS_Pos                  (2)
#define LPC_CAN_GSR_TBS_Msk                  (1UL << LPC_CAN_GSR_TBS_Pos)

#define LPC_CAN_GSR_TCS_Pos                  (3)
#define LPC_CAN_GSR_TCS_Msk                  (1UL << LPC_CAN_GSR_TCS_Pos)

#define LPC_CAN_GSR_RS_Pos                   (4)
#define LPC_CAN_GSR_RS_Msk                   (1UL << LPC_CAN_GSR_RS_Pos)

#define LPC_CAN_GSR_TS_Pos                   (5)
#define LPC_CAN_GSR_TS_Msk                   (1UL << LPC_CAN_GSR_TS_Pos)

#define LPC_CAN_GSR_ES_Pos                   (6)
#define LPC_CAN_GSR_ES_Msk                   (1UL << LPC_CAN_GSR_ES_Pos)

#define LPC_CAN_GSR_BS_Pos                   (7)
#define LPC_CAN_GSR_BS_Msk                   (1UL << LPC_CAN_GSR_BS_Pos)

#define LPC_CAN_GSR_RXERR_Pos                (16)
#define LPC_CAN_GSR_RXERR_Msk                (0xFFUL << LPC_CAN_GSR_RXERR_Pos)

#define LPC_CAN_GSR_TXERR_Pos                (24)
#define LPC_CAN_GSR_TXERR_Msk                (0xFFUL << LPC_CAN_GSR_TXERR_Pos)

/* CAN Interrupt and Capture Register */
#define LPC_CAN_ICR_RI_Pos                   (0)
#define LPC_CAN_ICR_RI_Msk                   (1UL << LPC_CAN_ICR_RI_Pos)

#define LPC_CAN_ICR_TI1_Pos                  (1)
#define LPC_CAN_ICR_TI1_Msk                  (1UL << LPC_CAN_ICR_TI1_Pos)

#define LPC_CAN_ICR_EI_Pos                   (2)
#define LPC_CAN_ICR_EI_Msk                   (1UL << LPC_CAN_ICR_EI_Pos)

#define LPC_CAN_ICR_DOI_Pos                  (3)
#define LPC_CAN_ICR_DOI_Msk                  (1UL << LPC_CAN_ICR_DOI_Pos)

#define LPC_CAN_ICR_WUI_Pos                  (4)
#define LPC_CAN_ICR_WUI_Msk                  (1UL << LPC_CAN_ICR_WUI_Pos)

#define LPC_CAN_ICR_EPI_Pos                  (5)
#define LPC_CAN_ICR_EPI_Msk                  (1UL << LPC_CAN_ICR_EPI_Pos)

#define LPC_CAN_ICR_ALI_Pos                  (6)
#define LPC_CAN_ICR_ALI_Msk                  (1UL << LPC_CAN_ICR_ALI_Pos)

#define LPC_CAN_ICR_BEI_Pos                  (7)
#define LPC_CAN_ICR_BEI_Msk                  (1UL << LPC_CAN_ICR_BEI_Pos)

#define LPC_CAN_ICR_IDI_Pos                  (8)
#define LPC_CAN_ICR_IDI_Msk                  (1UL << LPC_CAN_ICR_IDI_Pos)

#define LPC_CAN_ICR_TI2_Pos                  (8)
#define LPC_CAN_ICR_TI2_Msk                  (1UL << LPC_CAN_ICR_TI2_Pos)

#define LPC_CAN_ICR_TI3_Pos                  (10)
#define LPC_CAN_ICR_TI3_Msk                  (1UL << LPC_CAN_ICR_TI3_Pos)

#define LPC_CAN_ICR_ERRBIT_Pos               (16)
#define LPC_CAN_ICR_ERRBIT_Msk               (0x1FUL << LPC_CAN_ICR_ERRBIT_Pos)

#define LPC_CAN_ICR_ERRDIR_Pos               (21)
#define LPC_CAN_ICR_ERRDIR_Msk               (1UL << LPC_CAN_ICR_ERRDIR_Pos)

#define LPC_CAN_ICR_ERRC_Pos                 (22)
#define LPC_CAN_ICR_ERRC_Msk                 (3UL << LPC_CAN_ICR_ERRC_Pos)

#define LPC_CAN_ICR_ALCBIT_Pos               (24)
#define LPC_CAN_ICR_ALCBIT_Msk               (0xFFUL << LPC_CAN_ICR_ALCBIT_Pos)

/* CAN Interrupt Enable Register */
#define LPC_CAN_IER_RIE_Pos                  (0)
#define LPC_CAN_IER_RIE_Msk                  (1UL << LPC_CAN_IER_RIE_Pos)

#define LPC_CAN_IER_TIE1_Pos                 (1)
#define LPC_CAN_IER_TIE1_Msk                 (1UL << LPC_CAN_IER_TIE1_Pos)

#define LPC_CAN_IER_EIE_Pos                  (2)
#define LPC_CAN_IER_EIE_Msk                  (1UL << LPC_CAN_IER_EIE_Pos)

#define LPC_CAN_IER_DOIE_Pos                 (3)
#define LPC_CAN_IER_DOIE_Msk                 (1UL << LPC_CAN_IER_DOIE_Pos)

#define LPC_CAN_IER_WUIE_Pos                 (4)
#define LPC_CAN_IER_WUIE_Msk                 (1UL << LPC_CAN_IER_WUIE_Pos)

#define LPC_CAN_IER_EPIE_Pos                 (5)
#define LPC_CAN_IER_EPIE_Msk                 (1UL << LPC_CAN_IER_EPIE_Pos)

#define LPC_CAN_IER_ALIE_Pos                 (6)
#define LPC_CAN_IER_ALIE_Msk                 (1UL << LPC_CAN_IER_ALIE_Pos)

#define LPC_CAN_IER_BEIE_Pos                 (7)
#define LPC_CAN_IER_BEIE_Msk                 (1UL << LPC_CAN_IER_BEIE_Pos)

#define LPC_CAN_IER_IDIE_Pos                 (8)
#define LPC_CAN_IER_IDIE_Msk                 (1UL << LPC_CAN_IER_IDIE_Pos)

#define LPC_CAN_IER_TI2E_Pos                 (8)
#define LPC_CAN_IER_TI2E_Msk                 (1UL << LPC_CAN_IER_TI2E_Pos)

#define LPC_CAN_IER_TI3E_Pos                 (10)
#define LPC_CAN_IER_TI3E_Msk                 (1UL << LPC_CAN_IER_TI3E_Pos)

/* CAN Bus Timing Register */
#define LPC_CAN_BTR_BRP_Pos                  (0)
#define LPC_CAN_BTR_BRP_Msk                  (0x3FFUL << LPC_CAN_BTR_BRP_Pos)

#define LPC_CAN_BTR_SJW_Pos                  (14)
#define LPC_CAN_BTR_SJW_Msk                  (3UL << LPC_CAN_BTR_SJW_Pos)

#define LPC_CAN_BTR_TESG1_Pos                (16)
#define LPC_CAN_BTR_TESG1_Msk                (0xFUL << LPC_CAN_BTR_TESG1_Pos)

#define LPC_CAN_BTR_TESG2_Pos                (20)
#define LPC_CAN_BTR_TESG2_Msk                (7UL << LPC_CAN_BTR_TESG2_Pos)

#define LPC_CAN_BTR_SAM_Pos                  (23)
#define LPC_CAN_BTR_SAM_Msk                  (1UL << LPC_CAN_BTR_SAM_Pos)

/* CAN Error Warning Limit register */
#define LPC_CAN_EWL_EWL_Pos                  (0)
#define LPC_CAN_EWL_EWL_Msk                  (0xFFUL << LPC_CAN_EWL_EWL_Pos)

/* CAN Status Register */
#define LPC_CAN_SR_RBS_Pos                   (0)
#define LPC_CAN_SR_RBS_Msk                   (1UL << LPC_CAN_SR_RBS_Pos)

#define LPC_CAN_SR_DOS_Pos                   (1)
#define LPC_CAN_SR_DOS_Msk                   (1UL << LPC_CAN_SR_DOS_Pos)

#define LPC_CAN_SR_TBS1_Pos                  (2)
#define LPC_CAN_SR_TBS1_Msk                  (1UL << LPC_CAN_SR_TBS1_Pos)

#define LPC_CAN_SR_TCS1_Pos                  (3)
#define LPC_CAN_SR_TCS1_Msk                  (1UL << LPC_CAN_SR_TCS1_Pos)

#define LPC_CAN_SR_RS_Pos                    (4)
#define LPC_CAN_SR_RS_Msk                    (1UL << LPC_CAN_SR_RS_Pos)

#define LPC_CAN_SR_TS1_Pos                   (5)
#define LPC_CAN_SR_TS1_Msk                   (1UL << LPC_CAN_SR_TS1_Pos)

#define LPC_CAN_SR_ES_Pos                    (6)
#define LPC_CAN_SR_ES_Msk                    (1UL << LPC_CAN_SR_ES_Pos)

#define LPC_CAN_SR_BS_Pos                    (7)
#define LPC_CAN_SR_BS_Msk                    (1UL << LPC_CAN_SR_BS_Pos)

#define LPC_CAN_SR_TBS2_Pos                  (10)
#define LPC_CAN_SR_TBS2_Msk                  (1UL << LPC_CAN_SR_TBS2_Pos)

#define LPC_CAN_SR_TCS2_Pos                  (11)
#define LPC_CAN_SR_TCS2_Msk                  (1UL << LPC_CAN_SR_TCS2_Pos)

#define LPC_CAN_SR_TS2_Pos                   (13)
#define LPC_CAN_SR_TS2_Msk                   (1UL << LPC_CAN_SR_TS2_Pos)

#define LPC_CAN_SR_TBS3_Pos                  (18)
#define LPC_CAN_SR_TBS3_Msk                  (1UL << LPC_CAN_SR_TBS3_Pos)

#define LPC_CAN_SR_TCS3_Pos                  (19)
#define LPC_CAN_SR_TCS3_Msk                  (1UL << LPC_CAN_SR_TCS3_Pos)

#define LPC_CAN_SR_TS3_Pos                   (21)
#define LPC_CAN_SR_TS3_Msk                   (1UL << LPC_CAN_SR_TS3_Pos)

/* CAN Receive Frame Status register */
#define LPC_CAN_RFS_ID_Index_Pos            (0)
#define LPC_CAN_RFS_ID_Index_Msk            (0x3FFUL << LPC_CAN_RFS_ID_Index_Pos)

#define LPC_CAN_RFS_BP_Pos                  (10)
#define LPC_CAN_RFS_BP_Msk                  (1UL << LPC_CAN_RFS_BP_Pos)

#define LPC_CAN_RFS_DLC_Pos                 (16)
#define LPC_CAN_RFS_DLC_Msk                 (0xFUL << LPC_CAN_RFS_DLC_Pos)

#define LPC_CAN_RFS_RTR_Pos                 (30)
#define LPC_CAN_RFS_RTR_Msk                 (1UL << LPC_CAN_RFS_RTR_Pos)

#define LPC_CAN_RFS_FF_Pos                  (31)
#define LPC_CAN_RFS_FF_Msk                  (1UL << LPC_CAN_RFS_FF_Pos)

/* CAN Receive Identifier register */
#define LPC_CAN_RID_ID_Pos                  (0)
#define LPC_CAN_RID_ID_Msk                  (0x7FFUL << LPC_CAN_RID_ID_Pos)

#define LPC_CAN_RID_IDE_Pos                 (0)
#define LPC_CAN_RID_IDE_Msk                 (0x1FFFFFFFUL << LPC_CAN_RID_IDE_Pos)


/* CAN Transmit Frame Information register */
#define LPC_CAN_TFI_PRIO_Pos                (0)
#define LPC_CAN_TFI_PRIO_Msk                (0xFFUL << LPC_CAN_TFI_PRIO_Pos)

#define LPC_CAN_TFI_DLC_Pos                 (16)
#define LPC_CAN_TFI_DLC_Msk                 (0xFUL << LPC_CAN_TFI_DLC_Pos)

#define LPC_CAN_TFI_RTR_Pos                 (30)
#define LPC_CAN_TFI_RTR_Msk                 (1UL << LPC_CAN_TFI_RTR_Pos)

#define LPC_CAN_TFI_FF_Pos                  (31)
#define LPC_CAN_TFI_FF_Msk                  (1UL << LPC_CAN_TFI_FF_Pos)


/* CAN Transfer Identifier register */
#define LPC_CAN_TID_ID_Pos                  (0)
#define LPC_CAN_TID_ID_Msk                  (0x7FFUL << LPC_CAN_TID_ID_Pos)

#define LPC_CAN_TID_IDE_Pos                 (0)
#define LPC_CAN_TID_IDE_Msk                 (0x1FFFFFFFUL << LPC_CAN_TID_IDE_Pos)

/* CAN Sleep Clear register */
#define LPC_CAN_SLEEPCLR_CAN1SLEEP_Pos      (1)
#define LPC_CAN_SLEEPCLR_CAN1SLEEP_Msk      (1UL << LPC_CAN_SLEEPCLR_CAN1SLEEP_Pos)

#define LPC_CAN_SLEEPCLR_CAN2SLEEP_Pos      (2)
#define LPC_CAN_SLEEPCLR_CAN2SLEEP_Msk      (1UL << LPC_CAN_SLEEPCLR_CAN2SLEEP_Pos)

/* CAN Wake-up Flags register */
#define LPC_CAN_WAKEFLAGS_CAN1WAKE_Pos      (1)
#define LPC_CAN_WAKEFLAGS_CAN1WAKE_Msk      (1UL << LPC_CAN_WAKEFLAGS_CAN1WAKE_Pos)

#define LPC_CAN_WAKEFLAGS_CAN2WAKE_Pos      (2)
#define LPC_CAN_WAKEFLAGS_CAN2WAKE_Msk      (1UL << LPC_CAN_WAKEFLAGS_CAN2WAKE_Pos)

/* CAN Acceptance Filter Mode Register */
#define LPC_CAN_AFMR_AccOff_Pos             (0)
#define LPC_CAN_AFMR_AccOff_Msk             (1UL << LPC_CAN_AFMR_AccOff_Pos)

#define LPC_CAN_AFMR_AccBP_Pos              (1)
#define LPC_CAN_AFMR_AccBP_Msk              (1UL << LPC_CAN_AFMR_AccBP_Pos)

#define LPC_CAN_AFMR_eFCAN_Pos              (2)
#define LPC_CAN_AFMR_eFCAN_Msk              (1UL << LPC_CAN_AFMR_eFCAN_Pos)


#define LPC_CAN_SFF_sa_Pos                  (2)
#define LPC_CAN_SFF_sa_Msk                  (0x1FFUL << LPC_CAN_SFF_sa_Pos)

#define LPC_CAN_SFF_GRP_sa_Pos              (2)
#define LPC_CAN_SFF_GRP_sa_Msk              (0x3FFUL << LPC_CAN_SFF_GRP_sa_Pos)

#define LPC_CAN_EFF_sa_Pos                  (2)
#define LPC_CAN_EFF_sa_Msk                  (0x3FFUL << LPC_CAN_EFF_sa_Pos)

#define LPC_CAN_EFF_GRP_sa_Pos              (2)
#define LPC_CAN_EFF_GRP_sa_Msk              (0x3FFUL << LPC_CAN_EFF_GRP_sa_Pos)

#define LPC_CAN_ENDofTable_Pos              (2)
#define LPC_CAN_ENDofTable_Msk              (0x3FFUL << LPC_CAN_ENDofTable_Pos)
/*================================================================================================*/

#endif /*#ifndef IOLPC17XX_H_*/
